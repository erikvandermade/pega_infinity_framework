package DX;

import DX.DXAction;
import DX.DXField;
import DX.DXFieldType;
import DX.DXProperty;
import net.thucydides.core.annotations.Step;

public class DXStepsTemplate {

	public  DXAction DXaction;
	public  DXField filingStatus;	
	
	
	@Step
	public void i_check_the_property_of(DXField field, DXProperty property, int expectedValue) throws Exception
	{
		 switch (property)
		    {			
			case MaxLength:
				field.maxLength = expectedValue;
				DXaction.checkMaxLength(field);
				break;	
			case Value:
				field.value=String.valueOf(expectedValue);
				DXaction.checkValue(field);
				break;
		default:
			throw new Exception("Now check defined for " + property);
		    }	
	}
	
	public void refresh() 
	{	
		DXaction.refresh();
	}

	
	@Step
	public void i_check_the_property_of(DXField field, DXProperty property, String expectedValue) throws Exception
	{
			 switch (property)
			    {			
				case FieldType:
					DXFieldType fieldType = DXFieldType.get(expectedValue);
					field.fieldType=fieldType;
					DXaction.checkFieldType(field);
					break;
				case Label:
					field.label = expectedValue;
					DXaction.checkLabel(field);
					break;
				case Value:
					field.value=expectedValue;
					DXaction.checkValue(field);
					break;
			default:
				throw new Exception("Now check defined for " + property);
			    }			
	}
	
	@Step
	public void i_check_the_property_of(DXField field, DXProperty property, boolean expectedValue) throws Exception
	{
		System.err.println("======================= STARTING SUBSTEP: Checking " + property.name() + " of " + field.fieldID);
			 switch (property)
			    {			
				case ReadOnly:
					field.readOnly = expectedValue;
					DXaction.checkReadOnly(field);
					break;
				case Required:
					field.required = expectedValue;
					DXaction.checkRequired(field);
					break;
				case Visibility:
					field.visible = expectedValue;
					DXaction.checkVisibility(field);
					break;
				case Value:
					field.value=String.valueOf(expectedValue);
					DXaction.checkValue(field);
					break;	
			default:
				throw new Exception("Now check defined for " + property);
			    }			
			 System.err.println("======================= FINISHING SUBSTEP");
	}
	
	@Step
	protected void checkproperties(DXField field) throws Exception
	{
		System.err.println("======================= STARTING SUBSTEP: Checking properties of " + field.fieldID);
		DXaction.checkVisibility(field);
		if (field.visible)
				{
			DXaction.checkFieldType(field);
			DXaction.checkReadOnly(field);
			DXaction.checkRequired(field);
			DXaction.checkMaxLength(field);
			DXaction.checkValue(field);
			DXaction.checkLabel(field);
				}
		System.err.println("======================= FINISHING SUBSTEP");
	}
	
	protected void initializeDXField(DXField field, DXFieldType fieldType, boolean visible, boolean readonly, 
			boolean required, int maxLength, String label, String value) 
	{
		field.fieldType = fieldType;
		field.visible = visible;
		field.readOnly = readonly;
		field.required = required;
		field.maxLength = maxLength;
		field.label = label;
		field.value = value;	
	}
	


	
}
