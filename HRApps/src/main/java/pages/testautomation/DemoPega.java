package pages.testautomation;

import org.openqa.selenium.support.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import pega_objects.interfaces.WebElementPega;
import util.PageTemplate;

public class DemoPega extends PageTemplate {
	
	@FindBy(css="[data-test-id='2019050607491600324239']")
	protected WebElementPega demoRequired;
	@FindBy(css="[data-test-id='20190506075123066417154']")
	protected WebElementPega demoOptional;
	@FindBy(css="[data-test-id='20190506075307070933315']")
	protected WebElementPega demoShowHiddenField;
	@FindBy(css="[data-test-id='20190506075535095548926']")
	protected WebElementPega demoHiddenField;
	@FindBy(css="[data-test-id='20190506075834083969757']")
	protected WebElementPega demoInteger;
	

	public void setDemoRequired(String text)
	{			
		demoRequired.sendKeys(text);
	}
	
	public void demoOptional(String text)
	{			
		demoOptional.sendKeys(text);
	}
	
	public void clickShowHiddenField()
	{			
		demoShowHiddenField.click();
	}
	
	public void setDemoInteger(int value)
	{			
		demoInteger.sendKeys(String.valueOf(value));
	}

	public void checkDemoRequiredVisible(boolean visible) throws Exception
	{
		checkIfVisible(demoRequired, visible);
	}
	
	public void checkDemoOptionalVisible(boolean visible) throws Exception
	{
		checkIfVisible(demoOptional, visible);
	}
	
	public void checkDemoShowHiddenFieldDisplayed(boolean displayed) throws Exception
	{
		checkIfDisplayed(demoShowHiddenField, displayed);
	}
	
	public void checkDemoIntegerVisible(boolean visible) throws Exception
	{
		checkIfVisible(demoInteger, visible);
	}
	
	public void checkDemoHiddenFieldVisible(boolean visible) throws Exception
	{
		checkIfVisible(demoHiddenField, visible);
	}
	
	private void checkIfVisible(WebElementFacade element, boolean visible) throws Exception
	{
		if(!element.isVisible()==visible)	
		{
			throw new Exception("Element is not visible");
		}

	}
	
	private void checkIfDisplayed(WebElementFacade element, boolean displayed) throws Exception
	{
		if(!element.isDisplayed()==displayed)	
		{
			throw new Exception("Element is not visible");
		}

	}
	
		
}
