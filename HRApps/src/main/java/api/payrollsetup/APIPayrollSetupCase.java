package api.payrollsetup;

import org.json.JSONObject;

import api.APICase;
import enums.CaseType;

public class APIPayrollSetupCase extends APICase {

	CaseType caseType = CaseType.PayrollSetup;
	//WorkPage
	public String Withholding = "592.5000";
    public String FinalWithholding = "125.1000";
    public String FilingStatus = "Married";
    public String NumberOfDependents= "1";
    public String NumberOfExemptions= "3";
    public String RoutingNumber = "9999999999";
    public String AccountNumber = "2424242424";
    public String BankName = "PegaAPIBANK";
	//.Employee
	public String pyFirstName = "John";
	public String pyLastName = "Doe";
	public String Salary = "104000";
	public String BiweeklySalary = "4000";
	
	public void collectEmployeeInformation() throws Exception
	{
		JSONObject updateObject = new JSONObject();
			
		JSONObject employeeObject = new JSONObject();
		employeeObject.put("Salary", Salary);
		employeeObject.put("pyFirstName", pyFirstName);
		employeeObject.put("pyLastName", pyLastName);
		updateObject.put("Employee", employeeObject);
		updateCase(caseType, caseID, updateObject);		
	}
	
	public void submitCollectEmployeeInformation() throws Exception
	{
		performAssignmentAction("Collect employee information", "CollectEmployeeInformation_0", "");		
	}
	
	public void selectFilingStatus() throws Exception
	{
		JSONObject updateObject = new JSONObject();
		updateObject.put("FilingStatus", FilingStatus);
		updateCase(caseType, caseID, updateObject);		
	}

	public void submitSelectFilingStatus() throws Exception
	{
		performAssignmentAction("Select filing status", "SelectFilingStatus_0", "");		
	}
	
	public void calculateExemptions() throws Exception
	{
		JSONObject updateObject = new JSONObject();
		updateObject.put("FilingStatus", FilingStatus);
		updateObject.put("BiweeklySalary", BiweeklySalary);
	//	updateObject.put("FinalWithholding", FinalWithholding);
	    updateObject.put("NumberOfDependents", NumberOfDependents);
	    updateObject.put("NumberOfExemptions", NumberOfExemptions);
		updateCase(caseType, caseID, updateObject);		
	}

	public void submitCalculateExemptions() throws Exception
	{
		performAssignmentAction("Calculate exemptions", "CalculateExemptions_0", "");	
	}
	
	public void provideBankInformation() throws Exception
	{
		JSONObject updateObject = new JSONObject();
		updateObject.put("RoutingNumber", RoutingNumber);
		updateObject.put("AccountNumber", AccountNumber);
		updateObject.put("BankName", BankName);
		updateCase(caseType, caseID, updateObject);		
	}

	public void submitProvideBankInformation() throws Exception
	{
		performAssignmentAction("Provide bank information", "ProvideBankInformation_0", "");	
	}
		
}
