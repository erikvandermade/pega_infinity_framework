package pega_objects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import net.serenitybdd.core.annotations.findby.By;
import pega_objects.interfaces.RadioButtonTable;


public class RadioButtonTableImp extends WebElementPegaImp implements RadioButtonTable
{
	public RadioButtonTableImp(WebDriver driver, ElementLocator locator, long implicitTimeoutInMilliseconds,
			long waitForTimeoutInMilliseconds) {
		super(driver, locator, implicitTimeoutInMilliseconds, waitForTimeoutInMilliseconds);
	}


	@Override
	public void selectOption(String option) throws Exception {
		WebElement RadioButtonTable = getElement();
		List<WebElement> RadioButtons = RadioButtonTable.findElements(By.cssSelector("label"));
		boolean buttonFound = false;
		for (WebElement RadioButton:RadioButtons)
		{
			if (RadioButton.getText().equals(option))
			{
		
				RadioButton.click();
				buttonFound = true;
			}
		}
		if (buttonFound == false)
			{
				throw new Exception("RadioButton with text " + option + " not found.");
			}	
	}
	
	@Override
	public void checkOptions(List<String> options) throws Exception 
	{
		WebElement RadioButtonTable = getElement();
		List<WebElement> RadioButtons = RadioButtonTable.findElements(By.cssSelector("label"));
		if (!(options.size()==RadioButtons.size()))
		{
			throw new Exception(options.size() + " values are expected, while the radiobutton table has + " + RadioButtons.size() + " radiobuttons" );
		}
		for(String option: options)
		{		
			boolean buttonFound = false;
			for (WebElement radioButton: RadioButtons)
			{
				if (radioButton.getText().equals(option))
				{					
					buttonFound = true;
					break;
				}
			}
			if (buttonFound == false)
			{
				throw new Exception("RadioButton with text " + option + " not found.");
			}
		}
	}

}
