package pega_objects.interfaces;

import java.util.List;

import net.serenitybdd.core.annotations.ImplementedBy;
import pega_objects.DropdownImp;


@ImplementedBy(DropdownImp.class)
public interface Dropdown extends WebElementPega
{
	public void selectValue(String Value) throws Exception;
	void checkOptions(List<String> options) throws Exception;
	
}