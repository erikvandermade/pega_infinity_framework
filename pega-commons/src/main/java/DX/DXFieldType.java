package DX;

import java.util.HashMap;
import java.util.Map;

public enum DXFieldType
	
	{
		Dropdown ("pxDropdown"),
		TextInput ("pxTextInput"),
		Checkbox ("pxCheckbox"),
		Currency ("pxCurrency"),
		Integer ("pxInteger");

		public String value; 
 
		
		DXFieldType(String fieldValue)
		  {
		        this.value = fieldValue;
		  }
		
		public String value() 
		{ 
			return value; 
		}
		
		private static final Map<String, DXFieldType> lookup = new HashMap<>();
		
		static
		{
			 for(DXFieldType fieldType : DXFieldType.values())
		        {
		            lookup.put(fieldType.value(), fieldType);
		        }
		}
		
		public static DXFieldType get(String fieldType)
		{
			return lookup.get(fieldType);
		}
		
	}
	


