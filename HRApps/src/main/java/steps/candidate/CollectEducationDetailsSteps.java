package steps.candidate;

import net.thucydides.core.annotations.Step;
import pages.candidate.CollectEducationDetails;

public class CollectEducationDetailsSteps {

	CollectEducationDetails collectEducationDetails;

    @Step           
    public void i_fill_in_an_education1(String institution, String qualification, String yearOfCompletion) throws Exception 
    {
    	collectEducationDetails.type_Institution1(institution);
    	collectEducationDetails.type_Qualification1(qualification);
    	collectEducationDetails.type_YearOfCompletion1(yearOfCompletion);
    }
    
    @Step           
    public void i_fill_in_an_education2(String institution, String qualification, String yearOfCompletion) throws Exception 
    {
    	collectEducationDetails.click_addItem();
    	collectEducationDetails.type_Institution2(institution);
    	collectEducationDetails.type_Qualification2(qualification);
    	collectEducationDetails.type_YearOfCompletion2(yearOfCompletion);
    }
    
    

    
    
	
}
