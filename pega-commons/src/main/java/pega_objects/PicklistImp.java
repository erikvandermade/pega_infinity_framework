package pega_objects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import net.serenitybdd.core.annotations.findby.By;
import pega_objects.interfaces.Picklist;


public class PicklistImp extends WebElementPegaImp implements Picklist
{
	

	public PicklistImp(WebDriver driver, ElementLocator locator, long implicitTimeoutInMilliseconds,
			long waitForTimeoutInMilliseconds) {
		super(driver, locator, implicitTimeoutInMilliseconds, waitForTimeoutInMilliseconds);
	}




	@Override
	public void selectValue(String Value) throws Exception 
	{
		
		WebElement Picklist = getElement();
		List<WebElement> Options = Picklist.findElements(By.cssSelector("option"));
		boolean optionFound = false;
		for (WebElement Option:Options)
		{	
			if ((Option.getText().trim()).equals(Value))
			{
				Option.click();
				optionFound = true;
				break;
			}
		}
		if (!optionFound)
				{
					throw new Exception("Picklist option with text " + Value + " not found.");
				}

	}
	

	
		
	

}
