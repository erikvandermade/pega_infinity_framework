package steps.payrollsetup.DX;

import DX.DXAction;
import DX.DXField;
import DX.DXFieldType;
import DX.DXStepsTemplate;
import net.thucydides.core.annotations.Step;

public class DXSelectFilingStatusSteps extends DXStepsTemplate {

	public  DXField filingStatus;


	private void initialize()
	{
		DXaction = new DXAction("SelectFilingStatus_0");
		initializeFilingStatus();
	}

	@Step           
    public void i_check_that_the_initial_fields_have_the_correct_properties() throws Exception
    {
	 initialize();
	 checkproperties(filingStatus);
    }
	
	private void initializeFilingStatus() 
	{
		filingStatus = new DXField("FilingStatus");
		filingStatus.fieldType = DXFieldType.Dropdown;
		filingStatus.visible = true;
		filingStatus.readOnly = false;
		filingStatus.required = false;
		filingStatus.maxLength = 0;
		filingStatus.label = "Filing status";
		filingStatus.value = "";	
	}
	
}
