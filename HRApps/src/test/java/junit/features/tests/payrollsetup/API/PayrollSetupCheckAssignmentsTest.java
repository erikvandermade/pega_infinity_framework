package junit.features.tests.payrollsetup.API;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import api.APICase;
import api.payrollsetup.APIPayrollSetupSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.annotations.WithTags;
import util.casetypes.CurrentCase;

	@RunWith(SerenityRunner.class)
	@WithTags (
	        {
	        		@WithTag("PayrollSetupAPITests"),
	                @WithTag(name="Assignments", type="Payroll Setup"),
	                
	        }
	)
	public class PayrollSetupCheckAssignmentsTest 
	{
	@Steps
	APIPayrollSetupSteps payrollSetup;
	
	@Managed(driver = "chrome")
    WebDriver driver;
	
	@Before
	public void setup() 
	{
		APICase.setup();
	}

	@After
	public void deletecase() throws Exception 
	{
		APICase.deleteCase(APICase.caseWorkObject + " " + CurrentCase.caseID);
	}
	
	@Test
	public void Payroll_setup_check_that_select_filing_status_is_assigned_to_employee() throws Exception 
		{
			payrollSetup.i_have_a_payroll_setup_case_in_select_filing_status();
			payrollSetup.i_check_that_the_assignment_is_assigned_to("Select filing status", "TestEmployee");		
		}
	
	@Test
	public void Payroll_setup_check_that_calculate_exemptions_is_assigned_to_employee() throws Exception 
		{
			payrollSetup.i_have_a_payroll_setup_case_in_calculate_exemptions();
			payrollSetup.i_check_that_the_assignment_is_assigned_to("Calculate exemptions", "TestEmployee");	
		}
	
	@Test
	public void Payroll_setup_check_that_provide_bank_information_is_assigned_to_employee() throws Exception 
		{
			payrollSetup.i_have_a_payroll_setup_case_in_provide_bank_information();
			payrollSetup.i_check_that_the_assignment_is_assigned_to("Provide bank information", "TestEmployee");	
		}
	
	@Test
	public void Payroll_setup_check_that_review_payroll_setup_is_assigned_to_PayrollReviewWB() throws Exception 
		{
			payrollSetup.i_have_a_payroll_setup_case_in_review_payroll_setup();
			payrollSetup.i_check_that_the_assignment_is_assigned_to("Review selections", "PayrollReviewWB");	
		}
	
	}
	
	
	

