package steps.candidate;

import net.thucydides.core.annotations.Step;
import pages.candidate.CollectInformation;

public class CollectInformationSteps {

	CollectInformation collectInformation;

    @Step           
    public void i_fill_in_all_the_required_candidate_information(String position,
    		String firstName, String lastName, String email, String TIN, String countryCode) throws Exception 
    {  	
    	collectInformation.select_Position(position);
    	collectInformation.type_FirstName(firstName);
    	collectInformation.type_LastName(lastName);
    	collectInformation.type_Email(email);
    	collectInformation.type_TIN(TIN);
    	collectInformation.type_CountryCode(countryCode);  	
    }
    
    
	
}
