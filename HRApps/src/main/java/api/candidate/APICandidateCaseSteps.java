package api.candidate;

import DX.DXChecks;
import DX.DXFieldType;
import net.thucydides.core.annotations.Step;
import util.casetypes.CurrentCase;

public class APICandidateCaseSteps {

	APICandidateCase candidate;
	
	@Step
    public void new_candidate_case()
	{
		candidate = new APICandidateCase();
		candidate.createCase(candidate.caseType);
	}
	
	@Step
	public void collectPersonalDetails() throws Exception
	{
		candidate.collectInformation();
	}
	
	@Step
	public void submitCollectPersonalDetails() throws Exception
	{
		candidate.submitCollectInformation();
	
	}
	
	@Step
	public void collectWorkHistory() throws Exception
	{
		candidate.collectWorkHistory();
	}
	
	@Step
	public void submitCollectWorkHistory() throws Exception
	{
		candidate.submitcollectWorkHistory();	
	}
	
	@Step
	public void collectEducationHistory() throws Exception
	{
		candidate.collectEducationHistory();
	}
	
	@Step
	public void submitCollectEducationHistory() throws Exception
	{
		candidate.submitcollectEductionHistory();	
	}
	
	
	@Step
	public void i_check_that_there_are_attachments_attached_to_the_case(int nrOfAttachments) throws Exception
	{
		candidate.checknrOfAttachments(1);
	}
	
	
	@Step
	public void a_candidate_case_in_collect_work_sample() throws Exception
	{
		candidate = new APICandidateCase();
		candidate.createCase(candidate.caseType);
		System.err.println("======================= PROCESSING CASE TO COLLECT WORK SAMPLE");
		candidate.collectInformation();
		candidate.submitCollectInformation();
		candidate.collectWorkHistory();
		candidate.submitcollectWorkHistory();
		candidate.collectEducationHistory();
		
		candidate.submitcollectEductionHistory();
		System.err.println("======================= DONE");
		CurrentCase.assignmentHandle = candidate.getAssignmentID("Collect Work Sample");
	}
	
	@Step
	public void a_candidate_case_in_collect_information() throws Exception
	{
		candidate = new APICandidateCase();
		candidate.createCase(candidate.caseType);
		candidate.getAssignmentView("CollectPersonalDetails_0");	
	}
	
	@Step
	public void i_check_if_field_is_not_visible_properties(String fieldID) throws Exception
	{
		DXChecks.DXcheckFieldVisibleFromView(candidate.assignmentView, fieldID, false);
	}
	
	@Step
	public void i_check_the_visible_field_properties(String fieldID, boolean visible, boolean readOnly, boolean required, DXFieldType fieldType, int maxLength, String value, String label) throws Exception
	{
		DXChecks.DXcheckFieldVisibleFromView(candidate.assignmentView, fieldID, visible);
		DXChecks.DXcheckFieldLabelFromView(candidate.assignmentView, fieldID, label);
		DXChecks.DXcheckFieldMaxLengthFromView(candidate.assignmentView, fieldID, maxLength);
		DXChecks.DXcheckFieldReadOnlyFromView(candidate.assignmentView, fieldID, readOnly);
		DXChecks.DXcheckFieldRequiredFromView(candidate.assignmentView, fieldID, required);
		DXChecks.DXcheckFieldTypeFromView(candidate.assignmentView, fieldID, fieldType);
		DXChecks.DXcheckFieldValueFromView(candidate.assignmentView, fieldID, value);
		
	}
	


	
}
	
	
	
