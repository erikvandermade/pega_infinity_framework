package pega_objects.interfaces;

import net.serenitybdd.core.annotations.ImplementedBy;
import pega_objects.AutocompleteImp;

@ImplementedBy(AutocompleteImp.class)
public interface Autocomplete extends WebElementPega
{
	public void selectValue(String value);
	
}