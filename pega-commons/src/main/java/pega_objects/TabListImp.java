package pega_objects;

import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.ElementLocator;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.annotations.findby.By;
import pega_objects.interfaces.TabList;
import util.UtilActions;


public class TabListImp extends WebElementPegaImp implements TabList
{
	
	public TabListImp(WebDriver driver, ElementLocator locator, long implicitTimeoutInMilliseconds,
			long waitForTimeoutInMilliseconds) {
		super(driver, locator, implicitTimeoutInMilliseconds, waitForTimeoutInMilliseconds);
		// TODO Auto-generated constructor stub
	}

	
	@Override
	public void openTab(String TabName)
	{
		String xPathSelector = "//h2[contains(., '" + TabName + "')]";
		WebElement tab = getElement().findElement(By.xpath(xPathSelector));
		tab.click();
	}
	
	@Override
	public void checkTabPresent(String TabName) throws Exception
	{
		String xPathSelector = "//h2[contains(., '" + TabName + "')]";
		try
		{
		WebElement TaakElement = (new WebDriverWait(driver, 5))
				   .until(ExpectedConditions.presenceOfNestedElementLocatedBy(getElement(),By.xpath(xPathSelector)));
		UtilActions.scrollIntoView(TaakElement, driver);
		new WebDriverWait(driver, 1).until(ExpectedConditions.elementToBeClickable(TaakElement));
		}
		catch (Exception e)
		{
		throw new Exception ("Tab met naam <" + TabName + "> niet gevonden.");
		}
		
	}
	
	@Override
	public void checkTabNotPresent(String TabName) throws Exception
	{
		String xPathSelector = "//h2[contains(., '" + TabName + "')]";
		    try {
		        driver.findElement(By.xpath(xPathSelector));
		        throw new Exception ("Tab met naam <" + TabName + "> gevonden, terwijl dit niet wordt verwacht.");
		    } 
		    catch (NoSuchElementException ex) 
		    { 
		
		    }
	
		
	}

	@Override
	public void checkValueInTabForLabel(String TabName, String Label, String ExpectedResult) throws Exception
	{
		String xPathSelectorTab = "//h2[contains(., '" + TabName + "')]";
		WebElement tab = getElement().findElement(By.xpath(xPathSelectorTab));
		WebElement parent = tab.findElement(By.xpath(".."));
		WebElement grandParent = parent.findElement(By.xpath(".."));
		String xPathSelectorLabel = "//h2[contains(., '" + Label + "')]";
		WebElement label = grandParent.findElement(By.xpath(xPathSelectorLabel));
		WebElement ValueParent = label.findElement(By.xpath("/parent::div"));
		WebElement ValueCheck = ValueParent.findElement(By.cssSelector("a"));
		String ActualResult = ValueCheck.getText();		
		if (!ActualResult.equals(ExpectedResult))	
		{
			throw new Exception("Label bevat de waarde <" + ActualResult + ">, terwijl de "
					+ "waarde <" + ExpectedResult + "> wordt verwacht,");
		}
	}
	
	@Override
	public String returnValue(Integer TabNumber, String Label) throws Exception
	{
		WebElement ValueCheck = determineElement(TabNumber, Label);
		return ValueCheck.getText();		

	}
	
	@Override
	public void clickValue(Integer TabNumber, String Label) throws Exception
	{
		WebElement clickElement = determineElement(TabNumber, Label);
		clickElement.click();		

	}

	private WebElement determineElement(Integer TabNumber, String Label) throws Exception
	{
		String CssSelector = "div[role='tabpanel']";
		List<WebElement> tabs = getElement().findElements(By.cssSelector(CssSelector));
		Integer listSize = tabs.size()+1;
		System.out.println("Size: " + listSize);
		if (listSize < TabNumber)
		{
			throw new Exception ("Only " + listSize + " available, while item "+ TabNumber + " is expected.");
		}
		WebElement tabPanel = tabs.get(TabNumber-1);
		System.out.println("tabPanel: " + tabPanel.getAttribute("uniqueid" ));
		String xPathSelectorLabel = "//span[contains(text(),'" + Label + "')]";
		WebElement label = tabPanel.findElement(By.xpath(xPathSelectorLabel));
		WebElement ValueParent = label.findElement(By.xpath("ancestor::div[1]"));
		WebElement Element = ValueParent.findElement(By.cssSelector("a"));	
		return Element;
	}
}