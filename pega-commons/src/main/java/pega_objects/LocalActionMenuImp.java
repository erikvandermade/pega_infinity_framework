package pega_objects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import net.serenitybdd.core.annotations.findby.By;
import pega_objects.interfaces.LocalActionMenu;




public class LocalActionMenuImp extends WebElementPegaImp implements LocalActionMenu
{

	public LocalActionMenuImp(WebDriver driver, ElementLocator locator, long implicitTimeoutInMilliseconds,
			long waitForTimeoutInMilliseconds) {
		super(driver, locator, implicitTimeoutInMilliseconds, waitForTimeoutInMilliseconds);
	}

	public void selectMenuItem(String Text) throws Exception
	{
		WebElement Menu = getElement();
		List<WebElement> MenuItems = Menu.findElements(By.cssSelector("..menu-item-title"));
		boolean itemFound = false;
		for (WebElement Item : MenuItems)
		{
			if (Item.getText().equals(Text))
			{
				Item.click();
				break;
			}					
		}
		if (!itemFound)
		{
			throw new Exception("Item With Text '" + Text + "' not found");
		}
	}
	
			
		
}