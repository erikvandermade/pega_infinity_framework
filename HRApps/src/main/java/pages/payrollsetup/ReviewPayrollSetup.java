package pages.payrollsetup;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import net.serenitybdd.core.annotations.findby.By;
import pega_objects.interfaces.WebElementPega;
import util.PageTemplate;

public class ReviewPayrollSetup extends PageTemplate {
	
	@FindBy(css="[data-test-id='20160922090823085314573']")
	protected WebElementPega FirstName;
	@FindBy(css="[data-test-id='20160922090823085415817']")
	protected WebElementPega LastName;
	@FindBy(css="[data-test-id='20160922092532026052520'][data-ctl='Text']")
	protected WebElementPega Salary;
	@FindBy(css="[name='$PpyWorkPage$pBiweeklySalary']")
	protected WebElementPega BiWeeklySalary;
	@FindBy(css="[data-test-id='2016092210521008717827']")
	protected WebElementPega FilingStatus;
	@FindBy(css="[data-test-id='20160922105028009423109'][data-ctl='Text']")
	protected WebElementPega NrOfDependents;
	@FindBy(css="[name='$PpyWorkPage$pNumberOfExemptions']")
	protected WebElementPega NrOfExemptions;
	@FindBy(css="[name='$PpyWorkPage$pFinalWithholding']")
	protected WebElementPega FinalWithholding;
	@FindBy(css="ul[role='tablist']")
	protected WebElementPega Tablist;
	@FindBy(css="[data-test-id='2016092901401605711457']")
	protected WebElementPega BankName;
	@FindBy(css="[data-test-id='2016092901401605722574']")
	protected WebElementPega RoutingNumber;
	@FindBy(css="[data-test-id='2016092901401605723800']")
	protected WebElementPega AccountNumber;
	
	
	
	public void getPageFocus()
	{			
		getDriver().switchTo().defaultContent();
	}
	
	public void selectTab(String tabText)
	{			
		WebElement Tab = Tablist.findElement(By.xpath("//span[.='" + tabText + "']"));
		Tab.click();
	}
	
	public void checkFirstName(String expectedFirstName) throws Exception
	{
		String actualFirstName = FirstName.getText();
		if(!actualFirstName.equals(expectedFirstName))
				{
			throwExeption("First name", expectedFirstName, actualFirstName );
				}
	}
	
	public void checkLastName(String expectedLastName) throws Exception
	{
		String actualLastName = LastName.getText();
		if(!actualLastName.equals(expectedLastName))
				{
			throwExeption("Last name", expectedLastName, actualLastName );
				}
	}
	
	public void checkSalary(String expectedSalary) throws Exception
	{
		String actualSalary = Salary.getText();
		if(!actualSalary.equals(expectedSalary))
				{
			throwExeption("Salary", expectedSalary, actualSalary );
				}
	}
	
	public void checkBiWeeklySalary(String expectedBiweeklySalary) throws Exception
	{
		String actualBiWeeklySalary = BiWeeklySalary.getText();
		if(!actualBiWeeklySalary.equals(expectedBiweeklySalary))
				{
			throwExeption("Bi-weekly Salary", expectedBiweeklySalary, actualBiWeeklySalary );
				}
	}
	
	public void checkFilingStatus(String expectedFilingStatus) throws Exception
	{
		String actualFilingStatus = FilingStatus.getText();
		if(!actualFilingStatus.equals(expectedFilingStatus))
				{
			throwExeption("Filing Status", expectedFilingStatus, actualFilingStatus );
				}
	}
	
	public void checkNrOfDependents(String expectedNrOfDependents) throws Exception
	{
		String actualNrOfDependents = NrOfDependents.getText();
		if(!actualNrOfDependents.equals(expectedNrOfDependents))
				{
			throwExeption("Number of Dependents", expectedNrOfDependents, actualNrOfDependents );
				}
	}
	
	public void checkNrOfExemptions(String expectedNrOfExemptions) throws Exception
	{
		String actualNrOfExemptions = NrOfExemptions.getText();
		if(!actualNrOfExemptions.equals(expectedNrOfExemptions))
				{
			throwExeption("Number of Exemptions", expectedNrOfExemptions, actualNrOfExemptions );
				}
	}	
	
	public void checkFinalWithholding(String expectedFinalWithholding) throws Exception
	{
		String actualFinalWithholding = FinalWithholding.getText();
		if(!actualFinalWithholding.equals(expectedFinalWithholding))
				{
			throwExeption("Final Withholding", expectedFinalWithholding, actualFinalWithholding );
				}
	}	

	public void checkBankName(String expectedBankName) throws Exception
	{
		String actualBankName = BankName.getText();
		if(!actualBankName.equals(expectedBankName))
				{
			throwExeption("Bank name", expectedBankName, actualBankName );
				}
	}	
	
	public void checkRoutingNumber(String expectedRoutingNumber) throws Exception
	{
		String actualRoutingNumber = RoutingNumber.getText();
		if(!actualRoutingNumber.equals(expectedRoutingNumber))
				{
			throwExeption("Routing number", expectedRoutingNumber, actualRoutingNumber );
				}
	}	
	
	public void checkAccountNumber(String expectedAccountNumber) throws Exception
	{
		String actualAccountNumber = AccountNumber.getText();
		if(!actualAccountNumber.equals(expectedAccountNumber))
				{
			throwExeption("Account number", expectedAccountNumber, actualAccountNumber );
				}
	}
	
	
	private void throwExeption(String checked, String expectedValue, String actualValue) throws Exception
	{
		throw new Exception ("Actual " + checked + " = <" 
				+ actualValue + ">, while <" + 
				expectedValue + "> is expected.");
	}
	
	
		
}
