package angular.pages.testautomation;


import org.openqa.selenium.support.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import util.PageTemplate;

public class DemoAngular extends PageTemplate {
	
	@FindBy(css="[ng-reflect-name='DemoRequired']")
	protected WebElementFacade demoRequired;
	@FindBy(css="[ng-reflect-name='DemoOptional']")
	protected WebElementFacade demoOptional;
	@FindBy(css="[ng-reflect-id='DemoShowHiddenField']")
	protected WebElementFacade demoShowHiddenField;
	@FindBy(css="[ng-reflect-name='DemoHiddenField']")
	protected WebElementFacade demoHiddenField;
	@FindBy(css="[ng-reflect-name='DemoInteger']")
	protected WebElementFacade demoInteger;
	

	public void setDemoRequired(String text)
	{			
		demoRequired.sendKeys(text);
	}
	
	public void demoOptional(String text)
	{			
		demoOptional.sendKeys(text);
	}
	
	public void clickShowHiddenField()
	{			
		demoShowHiddenField.click();
	}
	
	public void setDemoInteger(int value)
	{			
		demoInteger.sendKeys(String.valueOf(value));
	}

	public void checkDemoRequiredVisible(boolean visible) throws Exception
	{
		checkIfVisible(demoRequired, visible);
	}
	
	public void checkDemoOptionalVisible(boolean visible) throws Exception
	{
		checkIfVisible(demoOptional, visible);
	}
	
	public void checkDemoShowHiddenFieldDisplayed(boolean displayed) throws Exception
	{
		checkIfDisplayed(demoShowHiddenField, displayed);
	}
	
	public void checkDemoIntegerVisible(boolean visible) throws Exception
	{
		checkIfVisible(demoInteger, visible);
	}
	
	public void checkDemoHiddenFieldVisible(boolean visible) throws Exception
	{
		checkIfVisible(demoHiddenField, visible);
	}
	
	private void checkIfVisible(WebElementFacade element, boolean visible) throws Exception
	{
		if(!element.isVisible()==visible)	
		{
			throw new Exception("Element is not visible");
		}

	}
	
	private void checkIfDisplayed(WebElementFacade element, boolean displayed) throws Exception
	{
		if(!element.isDisplayed()==displayed)	
		{
			throw new Exception("Element is not visible");
		}

	}
	
		
}
