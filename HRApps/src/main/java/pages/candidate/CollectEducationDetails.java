package pages.candidate;

import org.openqa.selenium.support.FindBy;

import pages.StandardPage;
import pega_objects.interfaces.WebElementPega;

public class CollectEducationDetails extends StandardPage
{
	@FindBy(css= "[data-test-id='2015071603054009744126']")
	protected WebElementPega addItem;
	@FindBy(css= "[data-test-id='201507160305400976873']")
	protected WebElementPega deleteItem;
	
	@FindBy(css= "[data-test-id='201904151134120538311'] tr:nth-of-type(2) [data-test-id='2015070709013409384376']")
	protected WebElementPega institution1;
	@FindBy(css= "[data-test-id='201904151134120538311'] tr:nth-of-type(2) [data-test-id='2015070709013409384377']")
	protected WebElementPega qualification1;
	@FindBy(css= "[data-test-id='201904151134120538311'] tr:nth-of-type(2) [data-test-id='2015070709013409384378']")
	protected WebElementPega yearOfCompletion1;
	
	@FindBy(css= "[data-test-id='201904151134120538311'] tr:nth-of-type(3) [data-test-id='2015070709013409384376']")
	protected WebElementPega institution2;
	@FindBy(css= "[data-test-id='201904151134120538311'] tr:nth-of-type(3) [data-test-id='2015070709013409384377']")
	protected WebElementPega qualification2;
	@FindBy(css= "[data-test-id='201904151134120538311'] tr:nth-of-type(3) [data-test-id='2015070709013409384378']")
	protected WebElementPega yearOfCompletion2;
	
	public void click_addItem()
	{
		addItem.click();
	}
	
	public void click_deleteItem()
	{
		deleteItem.click();
	}
	
	public void type_Institution1(String text)
	{
		institution1.type(text);
	}
	
	public void type_Qualification1(String text)
	{
		qualification1.type(text);
	}
	
	public void type_YearOfCompletion1(String text)
	{
		yearOfCompletion1.type(text);
	}
	
	public void type_Institution2(String text)
	{
		institution2.type(text);
	}
	
	public void type_Qualification2(String text)
	{
		qualification2.type(text);
	}
	
	public void type_YearOfCompletion2(String text)
	{
		yearOfCompletion2.type(text);
	}
	
	

}
