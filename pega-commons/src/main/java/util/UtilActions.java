package util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class UtilActions {
	

	public static void scrollIntoView(WebElement element, WebDriver driver) 
	{
		final String script =
				"var element = arguments[0];" +
				"element.scrollIntoView(true);";
			((JavascriptExecutor) driver).executeScript(script, element);
	}
	
	public static String toLowerCaseExceptStart(String string)
	{

		String lowerCaser = "";				
		int startPosition = 0;
		int position = string.indexOf(" ", startPosition);
		while (position > 0)
		{			
			if (position > 0)
			{	
				lowerCaser = lowerCaser + string.substring(startPosition, position);
				startPosition = position + 1;
				position = string.indexOf(" ", startPosition);
			}
			
		}
		lowerCaser = lowerCaser + string.substring(startPosition, string.length());
		return lowerCaser;
	}
	
	public static String toUpperCaseNoSpaces(String string)
	{
		string = string.replace(" ", "");
		return string.toUpperCase();
	}
	
	public static String toCamelCase(String string)
	{

		String CamelCaseString = "";				
		int startposition = 0;
		int position = string.indexOf(" ", startposition);
		while (position > 0)
		{			
			if (position > 0)
			{	
				CamelCaseString = CamelCaseString + string.substring(startposition, startposition + 1).toUpperCase() + string.substring(startposition + 1, position);
				startposition = position + 1;
				position = string.indexOf(" ", startposition);
			}
			
		}
		CamelCaseString = CamelCaseString + string.substring(startposition, startposition + 1).toUpperCase() + string.substring(startposition + 1, string.length());
		return CamelCaseString;
	}
	
	public static String toCamelCaseWithSpaces(String string)
	{

		String CamelCaseString = "";				
		int startposition = 0;
		int position = string.indexOf(" ", startposition);
		while (position > 0)
		{			
			if (position > 0)
			{	
				CamelCaseString = CamelCaseString + string.substring(startposition, startposition + 1).toUpperCase() + string.substring(startposition + 1, position) + " ";
				startposition = position + 1;
				position = string.indexOf(" ", startposition);
			}
			
		}
		CamelCaseString = CamelCaseString + string.substring(startposition, startposition + 1).toUpperCase() + string.substring(startposition + 1, string.length());
		return CamelCaseString;
	}
	
	public static String FromCamelCaseToSpaces(String string)
	{
		 String output = string.replaceAll(
			      String.format("%s|%s|%s",
			         "(?<=[A-Z])(?=[A-Z][a-z])",
			         "(?<=[^A-Z])(?=[A-Z])",
			         "(?<=[A-Za-z])(?=[^A-Za-z])"
			      ),
			      " "
			   );
		 output = output.substring(0,1)+output.substring(1, output.length()).toLowerCase();
		 return output;
	}
	
	
	public static String zuluTime()
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssSSS");
		Date date = new Date();
		String Datetime = (dateFormat.format(date)).toString() + "Z";
		return Datetime;
	}
	
	public static String Today_To_zuluTimeGMT()
	{
	    DateTimeFormatter DateFormat = DateTimeFormat.forPattern("yyyy-MM-dd");
	    LocalDate datetime = new LocalDate();
	    datetime = datetime.minusDays(1);
	    datetime = LocalDate.parse(datetime.toString(), DateFormat);
	    return datetime.toString()+"T23:00:00.000Z";
		
	}
	
	
	
	public static String GMT_to_Timezone(String dateTime, TimeZone timeZone, String format) throws ParseException
	{
		TimeZone.setDefault(TimeZone.getTimeZone("GMT")); 
		dateTime=dateTime.replace("T"," ");
		dateTime=dateTime.replace("Z","");
	    DateTimeFormatter dbDateFormat = DateTimeFormat
	            .forPattern(("yy-MM-dddd HH:mm:ss.SSS"));
	    DateTimeFormatter uiDateFormat = DateTimeFormat
	            .forPattern(format);
	    DateTime date = null;    
	    
	    int differenceGMT = timeZone.getOffset(System.currentTimeMillis())/3600000;
	    date = dbDateFormat.parseDateTime(dateTime);
	    date = date.plusHours(differenceGMT);
	    return uiDateFormat.print(date);  
	}
	
	public static String todayInYYYYMMDD()
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		Date date = new Date();
		String Date = dateFormat.format(date).toString();
		return Date;
	}
	
	public static String zuluTimeGMT()
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss.SSS");
		Date date = new Date();
		String Datetime = (dateFormat.format(date)).toString() + " GMT";
		return Datetime;
	}
	
	public static String booltoString(boolean bool)
	{
		if (bool)
		{
		return "true";
		}
		else
		{
			return "false";
		}
	}

	public static long getElementBottomPosition(WebElement element, WebDriver driver) 
	{
		
		final String getTopPositionScript = "var element = arguments[0];"
				+ "element.scrollIntoView(true);" +

				"var rect = element.getBoundingClientRect();" +

				"if (rect.width === 0 || rect.height === 0) {"
				+ "    console.warn('Element has a zero width / height');"
				+ "    return 0;" + "}" +

				"return Math.round(rect.bottom);";
		return (long) ((JavascriptExecutor) driver).executeScript(getTopPositionScript, element);
		
	}
	
	public static long getElementTopPosition(WebElement element, WebDriver driver) {
		
		final String getTopPositionScript = "var element = arguments[0];"
				+ "element.scrollIntoView(true);" +

				"var rect = element.getBoundingClientRect();" +

				"if (rect.width === 0 || rect.height === 0) {"
				+ "    console.warn('Element has a zero width / height');"
				+ "    return 0;" + "}" +

				"return Math.round(rect.top);";
		return (long) ((JavascriptExecutor) driver).executeScript(getTopPositionScript, element);
		
	}
	
	public static String dd_mm_yyyy_to_yyyymmdd(String dateString) throws Exception
	{
		Date date = new SimpleDateFormat("dd-MM-yyyy").parse(dateString);
		String newDateString = new SimpleDateFormat("yyyyMMdd").format(date);
		return newDateString;
	}
		
	public static int find_row_with_text(WebElement Table, int SearchInColumn, String text) throws Exception
	{
		WebElement GridTable = Table;
		List<WebElement> rows;
		try
		{
		rows = GridTable.findElements(By.cssSelector("tbody tr"));
		}
		catch (StaleElementReferenceException e)
		{
		rows = GridTable.findElements(By.cssSelector("tbody tr"));
		}
		int NrRows = rows.size(); 
		boolean rowFound = false;
		for (int searchRow=1; searchRow<(NrRows-1); searchRow++)		
		{
			int rowNum=searchRow+1;
			String cssSelector = "tbody tr:nth-child(" + rowNum + ") td:nth-child(" + SearchInColumn + ") span";
			WebElement CurrentRow = GridTable.findElement(By.cssSelector(cssSelector));
			System.out.println(rowNum);
			System.out.println(CurrentRow.getText());
			System.out.println(CurrentRow.getAttribute("class"));
			if (CurrentRow.getText().equals(text)) 
			{
				rowFound= true;
				return searchRow;
			}
					
		}
		if (rowFound == false)
		{
			throw new Exception("Row With Text '" + text + "' not found");
		}
		{	
			return 0;
		}
	}
	
}

