package pages;

import java.util.Base64;

import util.PageTemplate;
import util.Property;

public class OpenAssignment extends PageTemplate {
	
	private String baseUrl = Property.baseUrl;
	
	public void openAssignment(String assignmentHandle, String userName)
	{
		
		
		String userNameProperty = Property.LoadProperty("username_" + userName.toLowerCase());
		String passWordProperty = Property.LoadProperty("password_" + userName.toLowerCase());
		String passWordBase64 = Base64.getEncoder().encodeToString(passWordProperty.getBytes());
		String URL = baseUrl + "/prweb?pyActivity=pyMobileSnapStart&UserIdentifier=" + userNameProperty 
				+ "&Password=" + passWordBase64 + "&Action=openAssignment&InsHandle=" + assignmentHandle + "&pyShowFullPortal=true";
		getDriver().get(URL) ; 
		getDriver().manage().window().maximize();

	}
	
}
