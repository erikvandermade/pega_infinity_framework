package pega_objects.interfaces;

import net.serenitybdd.core.annotations.ImplementedBy;
import pega_objects.DatepickerImp;


@ImplementedBy(DatepickerImp.class)
public interface Datepicker extends WebElementPega
{
	public void selectToday();	
}
