package junit.features.tests.payrollsetup.UI;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import api.APICase;
import api.payrollsetup.APIPayrollSetupSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.annotations.WithTags;
import steps.general.InfoTabsSteps;
import steps.general.LoginSteps;
import steps.general.MyWorkSteps;
import steps.general.StandardSteps;
import steps.general.TaskWidgetSteps;
import steps.general.TopMenuSteps;
import steps.payrollsetup.CalculateExemptionsSteps;
import steps.payrollsetup.CollectEmployeeInformationSteps;
import steps.payrollsetup.ProvideBankInformationSteps;
import steps.payrollsetup.ReviewPayrollSetupSteps;
import steps.payrollsetup.SelectFilingStatusSteps;

	@RunWith(SerenityRunner.class)
	@WithTags (
	        {
	        		@WithTag("PayrollSetupUI"),
	                @WithTag(name="Check Steps", type="Payroll Setup"),
	                
	        }
	)
	public class PayrollSetupStep 
	{
	@Steps
	LoginSteps login;
	@Steps
	MyWorkSteps myWork;
	@Steps
	TopMenuSteps topMenu;
	@Steps
	StandardSteps standard;
	@Steps
	CollectEmployeeInformationSteps employee;
	@Steps
	SelectFilingStatusSteps filingStatus;
	@Steps
	CalculateExemptionsSteps exemptions;
	@Steps
	ProvideBankInformationSteps bankinformation;
	@Steps
	ReviewPayrollSetupSteps review;
	@Steps
	InfoTabsSteps infotabs;
	@Steps
	TaskWidgetSteps tasks;
	@Steps
	APIPayrollSetupSteps payrollSetup;
	
	@Managed(driver = "chrome")
    WebDriver driver;

	@Before
	public void setup() 
	{
		APICase.setup();
	}
	
	/*
	@Test
	public void CheckCalculateExemptionsStep() throws Exception 
	{
		payrollSetup.i_have_a_payroll_setup_case_in_calculate_exemptions();
		login.i_am_logged_in_as("standardUser");
		myWork.i_open_the_current_case_number();
		exemptions.i_fill_in_the_number_of_dependents(1);
		exemptions.i_check_the_calculated_number_of_exemptions(3);
		exemptions.i_check_the_calculated_final_withholding("€ 125,10");
	}
	*/
	
	}
	
	
	

