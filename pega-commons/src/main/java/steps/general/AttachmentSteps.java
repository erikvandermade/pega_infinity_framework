package steps.general;

import enums.Category;
import net.thucydides.core.annotations.Step;
import pages.Attachments;


public class AttachmentSteps {

	Attachments attachments;
	
    @Step
    public void there_are_attachments_with_category(Category category, int nrOfAttachments) throws Exception
    {
    	attachments.assertAttachmentsWithCategory(category, nrOfAttachments);
    }
    
    @Step
    public void there_are_attachment_attached(int nrOfAttachments) throws Exception
    {
    	attachments.assertVisibleAttachments(nrOfAttachments);
    }
    

}
