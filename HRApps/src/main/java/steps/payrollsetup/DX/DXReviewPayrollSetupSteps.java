package steps.payrollsetup.DX;

import DX.DXAction;
import DX.DXField;
import DX.DXFieldType;
import DX.DXStepsTemplate;
import net.thucydides.core.annotations.Step;

public class DXReviewPayrollSetupSteps extends DXStepsTemplate {

	public DXField FirstName = new DXField("pyFirstName");
	public DXField LastName = new DXField("pyLastName");
	public DXField Salary = new DXField("Salary");
	public DXField BiweeklySalary = new DXField("BiweeklySalary");
	public DXField FilingStatus = new DXField("FilingStatus");
	public DXField NumberOfDependents = new DXField("NumberOfDependents");
	public DXField NumberOfExemptions = new DXField("NumberOfExemptions");
	public DXField FinalWithholding = new DXField("FinalWithholding");
	public DXField BankName = new DXField("BankName");
	public DXField RoutingNumber = new DXField("RoutingNumber");
	public DXField AccountNumber = new DXField("AccountNumber");

	private void initialize()
	{
		DXaction = new DXAction("ReviewSelections_0");
		//initializeDXField(field, fieldType, visible, readonly, required, maxLength, label, value);
		initializeDXField(FirstName, DXFieldType.TextInput, true, true, false, 0, "First Name", "John");
		initializeDXField(LastName, DXFieldType.TextInput, true, true, false, 0, "Last Name", "Doe");
		initializeDXField(Salary, DXFieldType.Currency, true, true, false, -1, "Salary", "104000");
		initializeDXField(BiweeklySalary, DXFieldType.Currency, true, true, false, 0, "Bi-weekly salary", "4000");
		initializeDXField(FilingStatus, DXFieldType.Dropdown, true, true, false, 0, "Filing status", "Married");
		initializeDXField(NumberOfDependents, DXFieldType.Integer, true, true, true, 0, "Number of dependents", "1");
		initializeDXField(NumberOfExemptions, DXFieldType.Integer, true, true, false, 0, "Number of exemptions", "3");
		initializeDXField(FinalWithholding, DXFieldType.Currency, true, true, false, 0, "Final withholding", "125.10");
		initializeDXField(BankName, DXFieldType.TextInput, true, true, false, 0, "Bank name", "PegaAPIBANK");
		initializeDXField(RoutingNumber, DXFieldType.TextInput, true, true, true, 0, "Routing number", "9999999999");
		initializeDXField(AccountNumber, DXFieldType.TextInput, true, true, true, 0, "Account number", "2424242424");
		
		
		
	}

	@Step           
    public void i_check_that_the_fields_have_the_correct_properties() throws Exception
    {
	 initialize();
	 checkproperties(FirstName);
	 checkproperties(LastName);
	 checkproperties(Salary);
	 checkproperties(BiweeklySalary);
	 checkproperties(FilingStatus);
	 checkproperties(NumberOfDependents);
	 checkproperties(NumberOfExemptions);
	 checkproperties(FinalWithholding);	 
	 checkproperties(BankName);
	 checkproperties(RoutingNumber);
	 checkproperties(AccountNumber);
    }

	
	
	
	
}
