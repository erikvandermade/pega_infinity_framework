package steps.candidate;

import net.thucydides.core.annotations.Step;
import pages.candidate.CollectWorkSample;

public class CollectWorkSampleSteps {

	CollectWorkSample collectWorkSample;

    @Step           
    public void i_upload_a_work_sample() throws Exception 
    {  	
    	collectWorkSample.addFile("WorkSample.docx");
    }
    
    
	
}
