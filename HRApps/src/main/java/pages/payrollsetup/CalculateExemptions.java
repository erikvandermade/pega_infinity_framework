package pages.payrollsetup;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.FindBy;

import pega_objects.interfaces.Dropdown;
import pega_objects.interfaces.WebElementPega;
import util.PageTemplate;

public class CalculateExemptions extends PageTemplate {
	
	@FindBy(css="[data-test-id='20160922105028009423109']")
	protected Dropdown NumberOfDependents;
	@FindBy(css="[name='$PpyWorkPage$pNumberOfExemptions']")
	protected WebElementPega NumberOfExemptions;
	@FindBy(css="[name='$PpyWorkPage$pFinalWithholding']")
	protected WebElementPega FinalWithholding;
	
	public void getPageFocus()
	{			
		getDriver().switchTo().defaultContent();
	}
	public void typeNumberOfDependents(int nrOfDependents)
	{
		NumberOfDependents.type(String.valueOf(nrOfDependents));
		NumberOfDependents.sendKeys(Keys.TAB);
	}
	
	public void checkNumberOfExemptions(int expectedNrOfExemptions) throws Exception
	{
		String actualNrOfExemptions = NumberOfExemptions.getText();
		if(!actualNrOfExemptions.equals(String.valueOf(expectedNrOfExemptions)))
				{
				throw new Exception ("Actual number of exemptions = <" 
				+ String.valueOf(actualNrOfExemptions) + ">, while <" + 
						String.valueOf(expectedNrOfExemptions) + "> is expected.");
				}

	}
	
	public void checkFinalWitholding(String expectedFinalWithholding) throws Exception
	{
		String actualFinalWithholding = FinalWithholding.getText();
		if(!actualFinalWithholding.equals(expectedFinalWithholding))
				{
				throw new Exception ("Actual final withholding = <" 
				+ actualFinalWithholding + ">, while <" + 
						expectedFinalWithholding + "> is expected.");
				}

	}
	
	
	
		
}
