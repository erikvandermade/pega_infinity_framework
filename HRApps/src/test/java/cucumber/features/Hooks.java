package cucumber.features;

import cucumber.api.java.After;
import pages.LoginPage;
import pages.general.TopMenu;

public class Hooks {
	
	TopMenu topMenu;
	LoginPage loginPage;	
	
	
	@After(value="@UI")
    public void afterScenario(){
        topMenu.logOff();
        loginPage.waitForPageLoaded();
    }
}
