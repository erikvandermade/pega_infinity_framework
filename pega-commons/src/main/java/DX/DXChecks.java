package DX;

import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import api.APICase;
import util.Property;

public class DXChecks {

	public static String caseWorkObject = APICase.caseWorkObject;
	public String APIUsername = Property.LoadProperty("username_APITester");
	public String APIPassword = Property.LoadProperty("password_APITester");
	APICase apicase;


	
	public void checkCodeIsVisibleAndFilledWith(String code) throws Exception
	{
		JSONArray fields = new JSONArray();	
		System.out.println(fields);
		for (int i=0; i < fields.length(); i++) 
		{
			JSONObject fieldBody =  fields.getJSONObject(i);
			JSONObject fieldObject = fieldBody.getJSONObject("field");
			String actualFieldID = fieldObject.getString("fieldID");
			if (actualFieldID.equals("Code"))
			{
				String actualValue = fieldObject.getString("value");
				
						if (!actualValue.equals(code)){
							throw new Exception("Value is equal to <" + actualValue + ">, while it was not expected to be <" + code  + ">");
						}
			}
			else
			{
				continue;
			}
		}
	
	}
	
	public static void DXcheckFieldVisibleFromView(JSONObject assignmentView, String fieldID, boolean expectedVisibleValue) throws Exception
	{
		JSONObject field = getFieldFromView(assignmentView, fieldID);
		checkBooleanPropertyOfObject(field, "visible", expectedVisibleValue);		
	}
	
	public static void DXcheckFieldRequiredFromView(JSONObject assignmentView, String fieldID, boolean expectedRequiredValue) throws Exception
	{
		JSONObject field = getFieldFromView(assignmentView, fieldID);
		checkBooleanPropertyOfObject(field, "required", expectedRequiredValue);		
	}
	
	public static void DXcheckFieldValueFromView(JSONObject assignmentView, String fieldID, String expectedValue) throws Exception
	{
		JSONObject field = getFieldFromView(assignmentView, fieldID);
		checkStringPropertyOfObject(field, "value", expectedValue);		
	}
	
	public static void DXcheckFieldLabelFromView(JSONObject assignmentView, String fieldID, String expectedLabelValue) throws Exception 
	{
		JSONObject field = getFieldFromView(assignmentView, fieldID);
		if (!expectedLabelValue.equals(""))
			{
			try {
				checkBooleanPropertyOfObject(field, "showLabel", true);
			} catch (JSONException e) {
				// if showLabel doesn't exist, it will be shown, so it's all goooood
			} catch (Exception a){
				throw new Exception("Label is not visible, while it was expected to show <" + expectedLabelValue + ">");
			}
			
			checkStringPropertyOfObject(field, "label", expectedLabelValue);
			}
	}
	
	public static void DXcheckFieldReadOnlyFromView(JSONObject assignmentView, String fieldID, boolean expectedReadOnlyValue) throws Exception
	{
		JSONObject field = getFieldFromView(assignmentView, fieldID);
		checkBooleanPropertyOfObject(field, "readOnly", expectedReadOnlyValue);		
	}
	
	public static void DXcheckFieldMaxLengthFromView(JSONObject assignmentView, String fieldID, int expectedMaxLengthValue) throws Exception
	{
		JSONObject field = getFieldFromView(assignmentView, fieldID);
		checkIntPropertyOfObject(field, "maxLength", expectedMaxLengthValue);		
	}
	
	public static void DXcheckFieldTypeFromView(JSONObject assignmentView, String fieldID, DXFieldType expectedFieldTypeValue) throws Exception
	{
		JSONObject field = getFieldFromView(assignmentView, fieldID);
		JSONObject control = field.getJSONObject("control");
		checkStringPropertyOfObject(control, "type", expectedFieldTypeValue.value);		
	}
	
	private static void checkStringPropertyOfObject(JSONObject field, String key, String expectedValue) throws Exception 
	{
			
		String actualValue = String.valueOf(field.getString(key));
		if (!actualValue.equals(String.valueOf(expectedValue)))
				{
			throw new Exception(key + " is  equal to <" + actualValue + ">, while it was expected to be <" + expectedValue  + ">");
				}
		
	}
	
	private static void checkBooleanPropertyOfObject(JSONObject field, String key, boolean expectedValue) throws Exception 
	{
			
		boolean actualValue = field.getBoolean(key);
		if (!(actualValue==expectedValue))
				{
			throw new Exception(key + " is  equal to <" + String.valueOf(actualValue) + ">, while it was expected to be <" + String.valueOf(expectedValue)  + ">");
				}
		
	}

	private static void checkIntPropertyOfObject(JSONObject field, String key, int expectedValue) throws Exception 
	{
		int actualValue = field.getInt(key);
		if (!(actualValue==expectedValue))
				{
			throw new Exception(key + " is  equal to <" + String.valueOf(actualValue) + ">, while it was expected to be <" + String.valueOf(expectedValue)  + ">");
				};	
		
	}
	
	private static JSONObject getFieldFromView(JSONObject assignmentView, String fieldID) throws Exception
	{
		JSONArray fields = getFields(assignmentView);
		for (int i=0; i < fields.length(); i++) 
		
		{
			JSONObject fieldObject = fields.getJSONObject(i);
			String actualFieldID = fieldObject.getString("fieldID");
			if (actualFieldID.equals(fieldID))
			{
				return fieldObject;
			}
			else
			{
				continue;
			}
		}
		throw new Exception("Field <" + fieldID + "> not found");
	}
	
	private static JSONArray getFields(JSONObject view)
	{
		JsonParser p = new JsonParser();
		JSONArray fields = new JSONArray();
		JSONArray finalFields = getObjects(p.parse(view.toString()),fields, "field");
		return finalFields;
	}
	
	private static JSONArray getObjects(JsonElement jsonElement, JSONArray fields, String type) {	
        if (jsonElement.isJsonArray()) {
            for (JsonElement jsonElement1 : jsonElement.getAsJsonArray()) {
            	getObjects(jsonElement1,fields, type);
            }
        } else {
            if (jsonElement.isJsonObject()) {
                Set<Map.Entry<String, JsonElement>> entrySet = jsonElement
                        .getAsJsonObject().entrySet();
                for (Map.Entry<String, JsonElement> entry : entrySet) {
                    String key1 = entry.getKey();
                    if (key1.equals(type)) {             
                    	JSONObject fieldObject= new JSONObject(jsonElement.toString());
                    	fieldObject = fieldObject.getJSONObject("field");
                    	fields.put(fieldObject);            	
                    }
                    else
                    {
                    	getObjects(entry.getValue(),fields, type);
                    }
                }
            }
        }
		return fields;
    }
	
	
}
