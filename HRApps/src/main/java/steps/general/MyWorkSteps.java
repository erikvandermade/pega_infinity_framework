package steps.general;

import net.thucydides.core.annotations.Step;
import pages.general.MyWork;
import util.casetypes.CurrentCase;

public class MyWorkSteps {

	MyWork myWork;
	
	
	@Step           
    public void i_open_the_current_case_number() throws Exception
    {
		 myWork.openCasenumber(CurrentCase.caseID);
    }
	
}
