package steps.general;

import net.thucydides.core.annotations.Step;
import pages.general.TaskWidget;

public class TaskWidgetSteps {

	TaskWidget tasksWidget;
	
	
	@Step           
    public void i_open_the_assignment(String assignment) throws Exception
    {
		tasksWidget.openAssignment(assignment);
    }
	
}
