package junit.features.tests;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import DX.DXFieldType;
import api.APICase;
import api.candidate.APICandidateCaseSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.WithTag;
import steps.candidate.CollectWorkSampleSteps;
import steps.general.AttachmentSteps;
import steps.general.LoginSteps;
import steps.general.MyWorkSteps;
import steps.general.StandardSteps;
import util.casetypes.CurrentCase;

@RunWith(SerenityRunner.class)
@WithTag("APIDemo")

	public class TestFlowDXAPI
	{
		@Steps
		APICandidateCaseSteps candidate;
		@Steps
		LoginSteps login;
		@Steps
		MyWorkSteps myWork;
		@Steps
		CollectWorkSampleSteps workSample;
		@Steps
		AttachmentSteps attachments;
		@Steps
		StandardSteps standard;
		
		
		@Before
		public void setup() 
		{
			APICase.setup();
		}
		
		@After
		public void deletecase() throws Exception 
		{
			APICase.deleteCase(APICase.caseWorkObject + " " + CurrentCase.caseID);
		}
		
		@Managed(driver = "chrome")
	    WebDriver driver;
		

		@Test
		public void DX_Scan_first_Screen() throws Exception 
		{
			candidate.a_candidate_case_in_collect_information();	
			//candidate.i_check_the_visible_field_properties(fieldID, visible, readOnly, required, fieldType, maxLength, value, label);
			candidate.i_check_the_visible_field_properties("PositionAppliedFor", true, false, true, DXFieldType.Dropdown, 0, "", "Position applied for");
			candidate.i_check_the_visible_field_properties("pyLastName", true, false, true, DXFieldType.TextInput, 0, "", "Last Name");
			candidate.i_check_the_visible_field_properties("pyFirstName", true, false, false, DXFieldType.TextInput, 0, "", "First Name");
			candidate.i_check_the_visible_field_properties("ReferredByEmployee", true, false, false, DXFieldType.Checkbox, 0, "false", "");	
			candidate.i_check_the_visible_field_properties("pyTIN", true, false, true, DXFieldType.TextInput, 11, "", "Tax identification number (TIN)");	
			candidate.i_check_if_field_is_not_visible_properties("ReferringEmployee");

		}  		
	}




