package cucumber.features.steps.general.ui;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import enums.Category;
import net.thucydides.core.annotations.Step;
import pages.Attachments;
import pages.general.MyWork;
import util.casetypes.CurrentCase;

public class GeneralSteps {
	
	MyWork myWork;
	Attachments attachments;
	
    @When("I open the case")
    @Step
    public void I_Open_The_Case() throws Exception
    {
    	myWork.openCasenumber(CurrentCase.caseID);	
    }
    
    @Then("There are (\\d+) attachments of category \"(.*)\" uploaded to the case")
    @Step
    public void There_Are_Attachments(int nrOfAttachments, Category category) throws Exception
    {
    	attachments.assertAttachmentsWithCategory(category, nrOfAttachments);
    }

}
