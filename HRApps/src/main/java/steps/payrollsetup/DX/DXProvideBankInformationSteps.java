package steps.payrollsetup.DX;

import DX.DXAction;
import DX.DXField;
import DX.DXFieldType;
import DX.DXStepsTemplate;
import net.thucydides.core.annotations.Step;

public class DXProvideBankInformationSteps extends DXStepsTemplate {

	public DXField BankName = new DXField("BankName");
	public DXField RoutingNumber = new DXField("RoutingNumber");
	public DXField AccountNumber = new DXField("AccountNumber");


	private void initialize()
	{
		DXaction = new DXAction("ProvideBankInformation_0");
		//initializeDXField(field, fieldType, visible, readonly, required, maxLength, label, value);
		initializeDXField(BankName, DXFieldType.TextInput, true, false, false, 0, "Bank name", "");
		initializeDXField(RoutingNumber, DXFieldType.TextInput, true, false, true, 0, "Routing number", "");
		initializeDXField(AccountNumber, DXFieldType.TextInput, true, false, true, 0, "Account number", "");
	}

	@Step           
    public void i_check_that_the_initial_fields_have_the_correct_properties() throws Exception
    {
	 initialize();
	 checkproperties(BankName);
	 checkproperties(RoutingNumber);
	 checkproperties(AccountNumber);
    }
	
}
