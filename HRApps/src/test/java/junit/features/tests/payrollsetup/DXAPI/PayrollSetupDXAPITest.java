package junit.features.tests.payrollsetup.DXAPI;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import DX.DXProperty;
import api.APICase;
import api.payrollsetup.APIPayrollSetupSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.annotations.WithTags;
import steps.payrollsetup.DX.DXCalculateExemptionsSteps;
import steps.payrollsetup.DX.DXCollectEmployeeInformationSteps;
import steps.payrollsetup.DX.DXProvideBankInformationSteps;
import steps.payrollsetup.DX.DXReviewPayrollSetupSteps;
import steps.payrollsetup.DX.DXSelectFilingStatusSteps;
import util.casetypes.CurrentCase;

@RunWith(SerenityRunner.class)
@WithTags (
        {
        		@WithTag("PayrollSetupDXTests"),
        		@WithTag(name="DX", type="Payroll Setup"),
        }
        )

	public class PayrollSetupDXAPITest
	{

	@Steps
	APIPayrollSetupSteps payrollSetup;
	
	@Steps
	DXCollectEmployeeInformationSteps collectInformation;
	@Steps
	DXSelectFilingStatusSteps selectFilingStatus;
	@Steps
	DXCalculateExemptionsSteps calculateExemptions;
	@Steps
	DXProvideBankInformationSteps provideBankInformation;
	@Steps
	DXReviewPayrollSetupSteps reviewPayrollSetup;
	
	@Managed(driver = "chrome")
    WebDriver driver;
	
	@Before
	public void setup() 
	{
		APICase.setup();
	}

	@After
	public void deletecase() throws Exception 
	{
		APICase.deleteCase(APICase.caseWorkObject + " " + CurrentCase.caseID);
	}		
	
		@Test
		public void checkFieldsCollectEmployeeInformation() throws Exception 
		{
			payrollSetup.i_have_a_new_payroll_setup_case();
			collectInformation.i_check_that_the_initial_fields_have_the_correct_properties();
			payrollSetup.i_update_the_employee_information("Digital", "Experience", "26000");
			collectInformation.refresh();
			collectInformation.i_check_the_property_of(collectInformation.firstName, DXProperty.Value, "Digital");
			collectInformation.i_check_the_property_of(collectInformation.lastName, DXProperty.Value, "Experience");
			collectInformation.i_check_the_property_of(collectInformation.salary, DXProperty.Value, "26000");
			collectInformation.i_check_the_property_of(collectInformation.biWeeklySalary, DXProperty.Value, "1000.00");
		}  
		
		
		@Test
		public void checkFieldsSelectFilingStatus() throws Exception 
		{
			payrollSetup.i_have_a_payroll_setup_case_in_select_filing_status();
			selectFilingStatus.i_check_that_the_initial_fields_have_the_correct_properties();
			payrollSetup.i_update_the_filing_status("Married");
			selectFilingStatus.refresh();
			selectFilingStatus.i_check_the_property_of(selectFilingStatus.filingStatus, DXProperty.Value, "Married");			
		}  
		
		@Test
		public void checkFieldsCalculateExemptions() throws Exception 
		{
			payrollSetup.i_have_a_payroll_setup_case_in_calculate_exemptions();
			calculateExemptions.i_check_that_the_initial_fields_have_the_correct_properties();
			payrollSetup.i_update_the_number_of_dependents("1");
			calculateExemptions.refresh();
			calculateExemptions.i_check_the_property_of(calculateExemptions.NumberOfDependents, DXProperty.Value, "1");	
			calculateExemptions.i_check_the_property_of(calculateExemptions.NumberOfExemptions, DXProperty.Value, "3");		
			calculateExemptions.i_check_the_property_of(calculateExemptions.FinalWithholding, DXProperty.Value, "125.10");		
		} 
		
		@Test
		public void checkFieldsProvideBankInformation() throws Exception 
		{
			payrollSetup.i_have_a_payroll_setup_case_in_provide_bank_information();
			provideBankInformation.i_check_that_the_initial_fields_have_the_correct_properties();
			payrollSetup.i_update_the_bank_information("BankName", "123456789", "987654321");
			provideBankInformation.refresh();
			provideBankInformation.i_check_the_property_of(provideBankInformation.BankName, DXProperty.Value, "BankName");	
			provideBankInformation.i_check_the_property_of(provideBankInformation.RoutingNumber, DXProperty.Value, "123456789");		
			provideBankInformation.i_check_the_property_of(provideBankInformation.AccountNumber, DXProperty.Value, "987654321");		
		} 
		
		@Test
		public void checkFieldsReviewPayrollSetup() throws Exception 
		{
			payrollSetup.i_have_a_payroll_setup_case_in_review_payroll_setup();
			reviewPayrollSetup.i_check_that_the_fields_have_the_correct_properties();		
		} 
		
	}




