package api.calls;

public class PutCase {

		  public String ID;
		  public String actionID;
		  public String If_Match;
		  public String requestBody;

		  public void setID(String ID) 
		  {
		    this.ID = ID;
		  }

		  public void setactionID (String actionID) 
		  {
		    this.actionID = actionID;
		  }

		  public void setIf_Match(String If_Match) 
		  {
		    this.If_Match = If_Match;
		  }
		  
		  public void setIf_requestBody(String requestBody) 
		  {
		    this.requestBody = requestBody;
		  }

		  public String getID() 
		  {
			  return ID;
		  }

		  public String getactionID () 
		  {
			  return actionID;
		  }

		  public String getIf_Match() 
		  {
			  return If_Match;
		  }
		  
		  public String getIf_requestBody() 
		  {
			  return requestBody;
		  }


}
