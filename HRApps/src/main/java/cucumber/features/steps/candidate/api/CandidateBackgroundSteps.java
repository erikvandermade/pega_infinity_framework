package cucumber.features.steps.candidate.api;

import api.APICase;
import api.candidate.APICandidateCaseSteps;
import cucumber.api.java.Before;
import cucumber.api.java.en.When;
import enums.CaseType;
import enums.Status;
import net.thucydides.core.annotations.Step;

public class CandidateBackgroundSteps {
	
	APICandidateCaseSteps APICandidate;
	
	@Before
    public static void setupAPI(){
		APICase.setup();
    }
	
    @When("I have a \"(.*)\" case in status \"(.*)\"")
    @Step
    public void I_Have_A_Case_In_Status(CaseType caseType, Status status) throws Exception
    {
    	APICandidate = new APICandidateCaseSteps();
    	 if(status.equals(Status.UploadWorkSample))
	       {
    		 APICandidate.a_candidate_case_in_collect_work_sample();
	       }    	
    }

}
