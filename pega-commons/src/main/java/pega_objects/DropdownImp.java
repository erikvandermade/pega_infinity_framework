package pega_objects;

import java.util.List;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.pagefactory.ElementLocator;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.annotations.findby.By;
import pega_objects.interfaces.Dropdown;


public class DropdownImp extends WebElementPegaImp implements Dropdown
{


	public DropdownImp(WebDriver driver, ElementLocator locator, long implicitTimeoutInMilliseconds,
			long waitForTimeoutInMilliseconds) {
		super(driver, locator, implicitTimeoutInMilliseconds, waitForTimeoutInMilliseconds);
	}




	@Override
	public void selectValue(String Value) throws Exception 
	{
		
		WebElement Dropdown = getElement();
		new Actions(driver).moveToElement(Dropdown).perform();
		List<WebElement> Options = Dropdown.findElements(By.cssSelector("option"));
		boolean optionFound = false;
		for (WebElement Option:Options)
		{	
			if ((Option.getText().trim()).equals(Value))
			{
				getElement().click();
				WebElement clickOption = (new WebDriverWait(this.driver, 5))
						   .until(ExpectedConditions.elementToBeClickable(Option));
				clickOption.click();
				optionFound = true;
				try
				{
				new WebDriverWait(driver, 5)
				.until(ExpectedConditions.invisibilityOf(Option));
				}
				catch (StaleElementReferenceException e){}
				break;
			}
		}
		if (!optionFound)
				{
					throw new Exception("Dropdown option with text " + Value + " not found.");
				}
		}
	
		@Override
		public void checkOptions(List<String> options) throws Exception 
		{
			
			WebElement Dropdown = getElement();
			List<WebElement> values = Dropdown.findElements(By.cssSelector("option"));
			if (!(options.size()==values.size()))
			{
				throw new Exception(options.size() + " values are expected, while the dropdown has + " + values.size() + " options" );
			}
			for(String option: options)
			{		
				boolean optionFound = false;
				for (WebElement value: values)
				{
					if (value.getText().equals(option))
					{					
						optionFound = true;
						break;
					}
				}
				if (optionFound == false)
				{
					throw new Exception("Dropdown value <" + option + "> not found.");
				}
			}
	

	}
	

	
		
	

}
