package pega_objects;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import net.serenitybdd.core.annotations.findby.By;
import pega_objects.interfaces.RichTextEditor;


public class RichTextEditorImp extends WebElementPegaImp implements RichTextEditor
{

	private WebDriver driver;

	public RichTextEditorImp(WebDriver driver, ElementLocator locator, long implicitTimeoutInMilliseconds,
			long waitForTimeoutInMilliseconds) {
		super(driver, locator, implicitTimeoutInMilliseconds, waitForTimeoutInMilliseconds);
		this.driver = driver;
	}
	
			
	@Override
	public void typeText(String Text) {
		WebElement iframe = getElement();	
		driver.switchTo().frame(iframe);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		WebElement description = driver.findElement(By.cssSelector("body"));
		js.executeScript("arguments[0].innerHTML = arguments[1]", description, Text);
		driver.switchTo().defaultContent();	
	}
		

}
