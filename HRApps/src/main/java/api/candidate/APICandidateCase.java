package api.candidate;

import org.json.JSONArray;
import org.json.JSONObject;

import api.APICase;
import enums.CaseType;

public class APICandidateCase extends APICase {

	CaseType caseType = CaseType.Candidate;
	public String PositionAppliedFor = "REQ1";
	public String ProposedSalary = "120000";
	public String ReferredByEmployee = "false";
	public String Candidate_pyAddress = "Orteliuslaan 1000";	
	public String Candidate_pyCity = "Utrecht";	
	public String Candidate_pyCountryCode = "NLD";	
	public String Candidate_pyEmail1 = "email@valori.nl";	
	public String Candidate_pyFirstName = "Test";	
	public String Candidate_pyHomePhone = "0123456789";
	public String Candidate_pyLastName = "Valori";	
	public String Candidate_pyPostalCode = "3939AA";		
	public String Candidate_pyState = "UT";
	public String Candidate_pyTIN = "8987987";			

	
	public void collectInformation() throws Exception
	{
		JSONObject updateObject = new JSONObject();
			
		JSONObject candidateObject = new JSONObject();
		candidateObject.put("pyAddress", Candidate_pyAddress);
		candidateObject.put("pyCity", Candidate_pyCity);
		candidateObject.put("pyCountryCode", Candidate_pyCountryCode);
		candidateObject.put("pyEmail1", Candidate_pyEmail1);
		candidateObject.put("pyFirstName", Candidate_pyFirstName);
		candidateObject.put("pyHomePhone", Candidate_pyHomePhone);
		candidateObject.put("pyLastName", Candidate_pyLastName);
		candidateObject.put("pyPostalCode", Candidate_pyPostalCode);
		candidateObject.put("pyState", Candidate_pyState);
		candidateObject.put("pyTIN", Candidate_pyTIN);
		updateObject.put("Candidate", candidateObject);
		updateObject.put("PositionAppliedFor", PositionAppliedFor);
		updateObject.put("ProposedSalary", ProposedSalary);
		updateObject.put("ReferredByEmployee", ReferredByEmployee);
		
		updateCase(caseType, caseID, updateObject);
		
	}
	
	public void collectWorkHistory() throws Exception
	{
		
		JSONObject updateObject = new JSONObject();
			
		JSONArray experienceArray = new JSONArray();
		experienceArray.put(addWorkExperience("Job1","Company1","2010-01-01","2015-01-01"));
		experienceArray.put(addWorkExperience("Job2","Company2","2015-01-01","2018-01-01"));
		
		updateObject.put("Experience", experienceArray);
		updateCase(caseType, caseID, updateObject);	
	}
	
	public void collectEducationHistory() throws Exception
	{
		
		JSONObject updateObject = new JSONObject();
			
		JSONArray educationArray = new JSONArray();
		educationArray.put(addEducation("Institution1","Qualification1","2010"));
		educationArray.put(addEducation("Institution2","Qualification2","2015"));
		
		updateObject.put("Education", educationArray);
		
		updateCase(caseType, caseID, updateObject);	
	}
	
	public void submitCollectInformation() throws Exception
	{
		performAssignmentAction("Collect Personal Details", "CollectPersonalDetails_0", "");
		
	}

	public void submitcollectWorkHistory() throws Exception
	{
		performAssignmentAction("Collect Professional Details", "CollectProfessionalDetails_0", "");
	}
	
	public void submitcollectEductionHistory() throws Exception
	{
		performAssignmentAction("Collect Educational Details", "CollectEducationalDetails_0", "");
		
	}
	
	private JSONObject addWorkExperience(String jobTitle, String company, String startDate, String endDate)
	{
		JSONObject experienceObject= new JSONObject();
		experienceObject.put("pxObjClass", "TGB-HRApps-Data-Experience");
		experienceObject.put("JobTitle", jobTitle);
		experienceObject.put("Company", company);
		experienceObject.put("StartDate", startDate);
		experienceObject.put("EndDate", endDate);
		experienceObject.put("pxCreateDateTime", "2018-04-11T21:47:08.224Z");
		experienceObject.put("pxCreateOperator", "Bas");
		experienceObject.put("pxCreateOpName", "Bas");
		experienceObject.put("pxCreateSystemID", "pega");
		return experienceObject;

	}
	
	private JSONObject addEducation(String institution, String qualification, String yearOfCompletion)
	{
		JSONObject educationObject= new JSONObject();
		educationObject.put("pxObjClass", "TGB-HRApps-Data-Education");
		educationObject.put("Institution", institution);
		educationObject.put("Qualification", qualification);
		educationObject.put("YearOfCompletion", yearOfCompletion);
		educationObject.put("pxCreateDateTime", "2018-04-11T21:47:08.224Z");
		educationObject.put("pxCreateOperator", "Bas");
		educationObject.put("pxCreateOpName", "Bas");
		educationObject.put("pxCreateSystemID", "pega");
		return educationObject;

	}

		
}
