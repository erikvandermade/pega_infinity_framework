package pega_objects.interfaces;

import net.serenitybdd.core.annotations.ImplementedBy;
import pega_objects.LinkImp;


@ImplementedBy(LinkImp.class)
public interface Link extends WebElementPega
{

}