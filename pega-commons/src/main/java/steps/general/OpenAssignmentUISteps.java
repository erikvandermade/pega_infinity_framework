package steps.general;

import api.APICase;
import net.thucydides.core.annotations.Step;
import pages.LoginPage;
import pages.OpenAssignment;
import util.casetypes.CurrentCase;

public class OpenAssignmentUISteps {

	OpenAssignment assignment;
	APICase APICase;
	LoginPage login;

    @Step           
    public void i_open_case_and_assignment(String userName) throws Exception 
    {	  	
    	String AssignmentHandle = CurrentCase.assignmentHandle;
    	assignment.openAssignment(AssignmentHandle, userName);
    }
	
}
