package cucumber.features.steps.candidate.ui;

import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Step;
import pages.candidate.CollectWorkSample;

public class CollectWorkSampleSteps {
	
	CollectWorkSample collectWorkSample;
	
    @When("I upload \"(.*)\"")
    @Step
    public void I_Upload(String fileName) throws Exception
    {
    	collectWorkSample.addFile(fileName);
    }

}
