package angular.pages.general;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import net.serenitybdd.core.pages.WebElementFacade;
import util.PageTemplate;
import util.Property;

public class LoginPage extends PageTemplate {

	
	@FindBy(how = How.ID, using="userName")
	protected WebElementFacade userNameField;
	@FindBy(how = How.ID, using="password")
	protected WebElementFacade passwordField;
	@FindBy(how = How.XPATH, using="//*[text() = 'Login']")
	protected WebElementFacade loginButton;
	
	
	public void fillUserNameField(String userName)
	{			
		userNameField.waitUntilClickable();
		userNameField.sendKeys(userName);
	}
	
	public void clickLogin()
	{
		loginButton.click();
	}
	
	public void fillPasswordField(String password)
	{			
		passwordField.waitUntilClickable();
		passwordField.sendKeys(password);
	}
	
	public void Navigate()
	{
		getDriver().get("localhost:4200") ; 
		getDriver().manage().window().maximize();
		/*
		//When internet explorer: skip certification error:
			if (BrowserIsInternetExplorer())
		    {
				super.getDriver().navigate().to("javascript:document.getElementById('overridelink').click()");
		    }	
			// Continue
	*/	
	}
	
	public void login(String userName) throws Exception
	{
		try
		{
			fillUserNameField(Property.LoadProperty("username_" + userName.toLowerCase()));
		}
		catch (Exception e)
		{
			throw new Exception("config.properties does not include a username for \"" + userName + "\"."
					+ " Username must be specified as follows: <username_" + userName + "=[APPLICATIONUSERNAME]>");
	 
		}
		try
		{
			fillPasswordField(Property.LoadProperty("password_" + userName.toLowerCase()));
		}
		catch (Exception e)
		{
			throw new Exception("config.properties does not include a username for \"" + userName + "\"."
					+ " Password must be specified as follows: <password_" + userName + "=[password]>");
	 
		}
		
		clickLogin();
	}

}
