package pages.general;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import pega_objects.interfaces.WebElementPega;
import util.PageTemplate;

public class TopMenu extends PageTemplate {
	
	@FindBy(how = How.CSS, using="div[data-tour-id='cm-create-case-menu']")
	protected WebElementPega create;
	@FindBy(how = How.CSS, using = ".operator-menu:nth-of-type(1)")
	protected WebElementPega User;
	@FindBy(how = How.XPATH, using = "//span/span[.='Log off']")
	protected WebElementPega logOut;
	@FindBy(how = How.ID, using = "pySearchText")
	protected WebElementPega searchBar;
	//Below is different for each application
	@FindBy(how = How.XPATH, using = "//span/span[.='Candidate']")
	protected WebElementPega candidate;
	@FindBy(how = How.XPATH, using = "//span/span[.='Payroll Setup']")
	protected WebElementPega payrollsetup;
	@FindBy(how = How.XPATH, using = "//span/span[.='Testautomation']")
	protected WebElementPega testautomation;
	
	
	public void searchText(String Text)
	{
		searchBar.sendKeys(Text);
		searchBar.sendKeys(Keys.ENTER);
	}
	
	
	public void clickCreateCandidate()
	{
		create.click();
		candidate.waitUntilVisible();
		candidate.click();
		
		
	}
	
	public void logOff()
	{
		User.click();
		logOut.click();		
	}


	public void clickCreatePayrollSetup() {
		create.click();
		payrollsetup.waitUntilVisible();
		payrollsetup.click();		
	}
	
	public void clickCreateDemo() {
		create.click();
		testautomation.waitUntilVisible();
		testautomation.click();		
	}
}
