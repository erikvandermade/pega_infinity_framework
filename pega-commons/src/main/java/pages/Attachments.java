	package pages;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import enums.Category;
import net.serenitybdd.core.pages.WebElementFacade;
import pega_objects.interfaces.Dropdown;
import util.PageTemplate;

public class Attachments extends PageTemplate {
	
	@FindBy(how = How.CSS, using="a[aria-label='Menu Attach new']")
	protected WebElementFacade BijlagenPopup;
	@FindBy(how = How.CSS, using ="li[title='Bestand van apparaat']")
	protected WebElementFacade BestandVanApparaat;
	@FindBy(how = How.CSS, using="#modalContent #Browsefiles")
	protected WebElementFacade SelecteerBestandKnop;
	@FindBy(how = How.CSS, using="#modalContent #ModalButtonSubmit")
	protected WebElementFacade Toevoegenknop;
	@FindBy(how = How.CSS, using=".file-input-wrapper>input")
	protected WebElement BestandInput;
	@FindBy(how = How.CSS, using="#modalContent #pyCategory")
	protected Dropdown BestandsCategorie;
	@FindBy(how = How.CSS, using="#modalContent")
	protected WebElementFacade Popup;

	
	
		public void openBestandUploader() throws InterruptedException
	{
		BijlagenPopup.waitUntilClickable();
		BijlagenPopup.click();
		BestandVanApparaat.waitUntilClickable();
		BestandVanApparaat.click();
		SelecteerBestandKnop.waitUntilVisible();
	}
		
	public void addFile(Category Category, String fileName) throws Exception
	{	
		SelecteerBestandKnop.waitUntilVisible();
		String fileLocation = System.getProperty("user.dir") + "\\src\\test\\resources\\fileupload\\";
		super.getDriver().findElement(By.cssSelector("input[type='file']")).sendKeys(fileLocation + fileName);
		BestandsCategorie.selectByVisibleText(Category.label);
		Toevoegenknop.click();	
		(new WebDriverWait(super.getDriver(), 10))
				   .until(ExpectedConditions.elementToBeClickable(By.cssSelector("a[title='" + Category.label +"']")));
	
	}
		
	
	public void checkFileWithCategoryExists(Category Category) throws Exception
	{
		List<WebElement> Attachments = super.getDriver().findElements(By.cssSelector("div[data-node-id='pyCaseAttachments'] .subsidiary"));
		boolean optionFound = false;
		for (WebElement Attachment:Attachments)
		{	
			if (Attachment.getText().contains(Category.label))
			{
				optionFound = true;
				break;
			}
		}
		if (!optionFound)
				{
					throw new Exception("Attachment with categorie <" + Category.label + "> not found");
				}
		
	}
	
	public void assertAttachmentsWithCategory(Category category, int nrOfAttachments) throws Exception
	{
		int actualNrOfAttachements = countAttachmentsWithCategory(category);
		if (!(actualNrOfAttachements==nrOfAttachments))
		{
			throw new Exception(actualNrOfAttachements + " attachment(s) with Category <" + category.value +
					" > found, while " + nrOfAttachments + " where expected");
		}
	}
	
	public int countAttachmentsWithCategory(Category Category) throws Exception
	{
		List<WebElement> Attachments = super.getDriver().findElements(By.cssSelector("div[data-node-id='pyCaseAttachments'] .subsidiary"));
		int nrFound = 0;
		for (WebElement Attachment:Attachments)
		{	
			if (Attachment.getText().contains(Category.value))
			{
				nrFound = nrFound + 1;
				
			}
		}
		return nrFound;
		
	}
	
	public int countVisibleAttachments() throws Exception
	{
		List<WebElement> Attachments = super.getDriver().findElements(By.cssSelector("[data-test-id^='201808170838240366653']"));
		//List<WebElement> Attachments = super.getDriver().findElements(By.cssSelector("[data-test-id^='201801030904400319962-']"));
		return Attachments.size();		
	}
	
	public void assertVisibleAttachments(int nrOfAttachments) throws Exception
	{
		int actualNrOfAttachments = countVisibleAttachments();
		if (!(actualNrOfAttachments==nrOfAttachments))
		{
			throw new Exception(actualNrOfAttachments + " attachment(s) found, while " + nrOfAttachments + " where expected");
		}
	}

	public void checkAttachmentsInCorrespondence(String correspondenceTitle, Category category) throws Exception 
	{
		List<WebElement> Attachments = super.getDriver().findElements(By.cssSelector("div[data-node-id='pyCaseAttachments'] .Case_tools"));;
		int counter = 0;	
		for (WebElement Attachment:Attachments)
			{	
				if (Attachment.getAttribute("title").equals(correspondenceTitle))
				{
					counter = counter + 1;
					checkCorrespondenceAttachment(Attachment, category);	
				}
			}
		if (counter==0)
		{
			throw new Exception("No Correspondence found with <" + correspondenceTitle + "> as its title");
		}
	}
	
	
	

	public void checkTextInCorrespondence(String correspondenceTitle, String text) throws Exception 
	{
		List<WebElement> Attachments = super.getDriver().findElements(By.cssSelector("div[data-node-id='pyCaseAttachments'] .Case_tools"));;
		int counter = 0;	
		for (WebElement Attachment:Attachments)
			{	
				if (Attachment.getAttribute("title").equals(correspondenceTitle))
				{
					counter = counter + 1;
					CheckCorrespondenceBodyText(Attachment, text);	
				}
			}
		if (counter==0)
		{
			throw new Exception("No Correspondence found with <" + correspondenceTitle + "> as its title");
		}
		
	}
	
	public void checkTextNotInCorrespondence(String correspondenceTitle, String text) throws Exception 
	{
		List<WebElement> Attachments = super.getDriver().findElements(By.cssSelector("div[data-node-id='pyCaseAttachments'] .Case_tools"));;
		int counter = 0;	
		for (WebElement Attachment:Attachments)
			{	
				if (Attachment.getAttribute("title").equals(correspondenceTitle))
				{
					counter = counter + 1;
					CheckNotCorrespondenceBodyText(Attachment, text);	
				}
			}
		if (counter==0)
		{
			throw new Exception("No Correspondence found with <" + correspondenceTitle + "> as its title");
		}
		
	}
	
	private void checkCorrespondenceAttachment(WebElement attachment, Category category) throws Exception 
	{
		WebDriver driver = getDriver();
		String parentWindowHandler = driver.getWindowHandle(); // Store your parent window
		String subWindowHandler = null;
		
		attachment.click();//opens window
		waitForNumberOfWindowsToEqual(2);
		Set<String> handles = driver.getWindowHandles();// get all window handles

		Iterator<String> iterator = handles.iterator();
		while (iterator.hasNext()){
		    subWindowHandler = iterator.next();
		}
		driver.switchTo().window(subWindowHandler); // switch to popup window

		try
		{
			driver.findElement(By.xpath("//a[contains(text(),'"+ category.label + "')]"));
		}
		catch (Exception e)
		{
			driver.switchTo().window(parentWindowHandler);
			throw new Exception ("Attachment with text <" + category.label + "> not found");
		}

		driver.switchTo().window(parentWindowHandler);  // switch back to parent window
		

		
	}
	


	private void CheckCorrespondenceBodyText(WebElement attachment, String text) throws Exception 
	{
		WebDriver driver = getDriver();
		String parentWindowHandler = driver.getWindowHandle(); // Store your parent window
		String subWindowHandler = null;
		
		attachment.click();//opens window
		waitForNumberOfWindowsToEqual(2);
		Set<String> handles = driver.getWindowHandles(); // get all window handles
		Iterator<String> iterator = handles.iterator();
		while (iterator.hasNext()){
		    subWindowHandler = iterator.next();
		}
		driver.switchTo().window(subWindowHandler); // switch to popup window
		try
		{
			driver.findElement(By.xpath("//textarea[contains(text(),'" + text + "')]"));
		}
		catch (Exception e)
		{
			driver.switchTo().window(parentWindowHandler);
			throw new Exception ("Text <" + text + "> not found in correspondence");
		}

		driver.switchTo().window(parentWindowHandler);  // switch back to parent window
			
	}
	
	private void CheckNotCorrespondenceBodyText(WebElement attachment, String text) throws Exception 
	{
		WebDriver driver = getDriver();
		String parentWindowHandler = driver.getWindowHandle(); // Store your parent window
		String subWindowHandler = null;
		
		attachment.click();//opens window
		waitForNumberOfWindowsToEqual(2);
		Set<String> handles = driver.getWindowHandles(); // get all window handles
		System.out.println("Grootte: " + handles.size());
		Iterator<String> iterator = handles.iterator();
		while (iterator.hasNext()){
		    subWindowHandler = iterator.next();
		}
		driver.switchTo().window(subWindowHandler); // switch to popup window
		try
		{
			driver.findElement(By.xpath("//textarea[contains(text(),'" + text + "')]"));
			throw new Exception ("Text <" + text + "> found in correspondence, while it was not expected");
		}
		catch (Exception e)
		{
			
		}	
		driver.switchTo().window(parentWindowHandler);  // switch back to parent window			
	}
	
	private void waitForNumberOfWindowsToEqual(final int numberOfWindows) 
	{    
		new WebDriverWait(getDriver(), 5) {
	    }.until(new ExpectedCondition<Boolean>() {
	        @Override
	        public Boolean apply(WebDriver driver) {                        
	            return (driver.getWindowHandles().size() == numberOfWindows);
	        }
	    });
	}


		
}
	
	