package pega_objects.interfaces;

import net.serenitybdd.core.annotations.ImplementedBy;
import net.serenitybdd.core.pages.WebElementFacade;
import pega_objects.WebElementPegaImp;

@ImplementedBy(WebElementPegaImp.class)
public interface WebElementPega extends WebElementFacade
{
	public void click();
	
}