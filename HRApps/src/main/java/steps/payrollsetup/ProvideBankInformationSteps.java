package steps.payrollsetup;

import net.thucydides.core.annotations.Step;
import pages.payrollsetup.ProvideBankInformation;

public class ProvideBankInformationSteps {

	ProvideBankInformation provideBankInformation;
	
	@Step           
    public void i_fill_in_the_bank_information(String bankName, String routingNumber, String accountNumber) throws Exception
    {
		provideBankInformation.typeBankName(bankName);
		provideBankInformation.typeRoutingNumber(routingNumber);
		provideBankInformation.typeAccountNumber(accountNumber);		
    }
	
	

	
	
	
	
}
