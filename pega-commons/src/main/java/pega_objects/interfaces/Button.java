package pega_objects.interfaces;

import net.serenitybdd.core.annotations.ImplementedBy;
import pega_objects.ButtonImp;


@ImplementedBy(ButtonImp.class)
public interface Button extends WebElementPega
{

	
}