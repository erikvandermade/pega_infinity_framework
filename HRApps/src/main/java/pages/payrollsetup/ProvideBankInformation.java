package pages.payrollsetup;

import org.openqa.selenium.support.FindBy;

import pega_objects.interfaces.WebElementPega;
import util.PageTemplate;

public class ProvideBankInformation extends PageTemplate {
	
	@FindBy(css="[data-test-id='2016092901401605711457']")
	protected WebElementPega BankName;
	@FindBy(css="[data-test-id='2016092901401605722574']")
	protected WebElementPega RoutingNumber;
	@FindBy(css="[data-test-id='2016092901401605723800']")
	protected WebElementPega AccountNumber;
	
	public void getPageFocus()
	{			
		getDriver().switchTo().defaultContent();
	}
	public void typeBankName(String bankName)
	{
		BankName.type(bankName);
	}
	
	public void typeRoutingNumber(String routingNumber)
	{
		RoutingNumber.type(routingNumber);
	}
	
	public void typeAccountNumber(String accountNumber)
	{
		AccountNumber.type(accountNumber);
	}
	

	

	
	
	
		
}
