package pega_objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import net.serenitybdd.core.annotations.findby.By;
import pega_objects.interfaces.Datepicker;


public class DatepickerImp extends WebElementPegaImp implements Datepicker
{
	


	public DatepickerImp(WebDriver driver, ElementLocator locator, long implicitTimeoutInMilliseconds,
			long waitForTimeoutInMilliseconds) {
		super(driver, locator, implicitTimeoutInMilliseconds, waitForTimeoutInMilliseconds);
	}
	




	@Override
	public void selectToday() {

		WebElement todayCell = getElement().findElement(By.cssSelector("#todayLink"));
		todayCell.click();
		
		
	}
	

	
		
	

}
