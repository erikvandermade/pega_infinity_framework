package junit.features.tests.testautomation.DXAPI;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import DX.DXProperty;
import angular.steps.general.AngularLoginSteps;
import angular.steps.general.AngularTopMenuSteps;
import angular.steps.testautomation.AngularTestautomationSteps;
import api.APICase;
import api.testautomation.APITestautomationCaseSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.WithTag;
import steps.general.LoginSteps;
import steps.general.TopMenuSteps;
import steps.testautomation.TestautomationSteps;
import steps.testautomation.DX.DXDemoSteps;

@RunWith(SerenityRunner.class)
@WithTag("APIDemo")

	public class TestautomationDXAPITests
	{

	@Steps
	APITestautomationCaseSteps testAutomation;
	@Steps
	DXDemoSteps DXCheck;
	@Steps
	AngularLoginSteps angularLogin;
	@Steps
	AngularTopMenuSteps angularTopMenu;
	@Steps
	AngularTestautomationSteps angularDemo;
	@Steps
	LoginSteps login;
	@Steps
	TopMenuSteps topMenu;
	@Steps
	TestautomationSteps demoPega;
	
	
	
	@Managed(driver = "chrome")
    WebDriver driver;
	
	@Before
	public void setup() 
	{
		APICase.setup();
	}

	@After
	public void deletecase() throws Exception 
	{
		//APICase.deleteCase(APICase.caseWorkObject + " " + CurrentCase.caseID);
	}		
	
	
	
		@Test
		public void checkFieldsCollectEmployeeInformation() throws Exception 
		{
			testAutomation.i_have_a_new_demo_case();
			DXCheck.i_check_that_the_initial_fields_have_the_correct_properties();
			testAutomation.i_update_the_demo_fields("", 42, "", "", true);
			DXCheck.refresh();
//			DXCheck.i_check_the_property_of(DXCheck.DemoHiddenField, DXProperty.Value, "Hidden Field");	
			DXCheck.i_check_the_property_of(DXCheck.DemoHiddenField, DXProperty.Visibility, true);
//			DXCheck.i_check_the_property_of(DXCheck.DemoInteger, DXProperty.Value, 42);	
//			DXCheck.i_check_the_property_of(DXCheck.DemoOptional, DXProperty.Value, "Optional Field");	
//			DXCheck.i_check_the_property_of(DXCheck.DemoRequired, DXProperty.Value, "Required Field");	
//			DXCheck.i_check_the_property_of(DXCheck.DemoShowHiddenField, DXProperty.Value, true);			

		}  
		
		//@Test
		public void checkFieldsCollectEmployeeInformationOnAngular() throws Exception 
		{
			angularLogin.i_login();
			angularTopMenu.i_create_a_demo_case();
			angularDemo.i_check_the_visibility_of_the_initial_fields();
			angularDemo.i_click_the_show_hidden_field_checkbox();
			angularDemo.i_check_if_the_hidden_field_is_visible(true);

		}  
		
		//@Test
		public void checkFieldsCollectEmployeeInformationOnPega() throws Exception 
		{
			login.i_am_logged_in_as("standardUser");
			topMenu.i_create_a_demo_case();
			demoPega.i_check_the_visibility_of_the_initial_fields();
			demoPega.i_click_the_show_hidden_field_checkbox();
			demoPega.i_check_if_the_hidden_field_is_visible(true);

		}  
		
		
	
	}




