package pages;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import pega_objects.interfaces.WebElementPega;
import util.PageTemplate;
import util.Property;

public class LoginPage extends PageTemplate
{
	
	private String baseUrl = Property.baseUrl;
	
	// declaration of page objects
	@FindBy(how = How.ID, using = "txtUserID")
	private WebElementPega userIDField;
	@FindBy(how = How.ID, using = "txtPassword")
	private WebElementPega passwordField;
	@FindBy(how = How.ID, using = "sub")
	private WebElementPega loginButton;
	@FindBy(how = How.ID, using = "errorDiv")
	private WebElementPega errorMessage;
	
	public void NavigateAndlogin(String username) throws Exception
	{

		navigate();
		login(username);
	
	}
	
	public void login(String userName) throws Exception
	{
		try
		{
			fillUserIDField(Property.LoadProperty("username_" + userName.toLowerCase()));
		}
		catch (Exception e)
		{
			throw new Exception("config.properties does not include a username for \"" + userName + "\"."
					+ " Username must be specified as follows: <username_" + userName + "=[APPLICATIONUSERNAME]>");
	 
		}
		try
		{
			fillPasswordField(Property.LoadProperty("password_" + userName.toLowerCase()));
		}
		catch (Exception e)
		{
			throw new Exception("config.properties does not include a username for \"" + userName + "\"."
					+ " Password must be specified as follows: <password_" + userName + "=[password]>");
	 
		}
		
		clickSubmit();	
	}
	
	public void navigate()
	{
		getDriver().get(baseUrl + "/prweb/") ; 
		getDriver().manage().window().maximize();
		/*
		//When internet explorer: skip certification error:
			if (BrowserIsInternetExplorer())
		    {
				super.getDriver().navigate().to("javascript:document.getElementById('overridelink').click()");
		    }	
			// Continue
	*/	
	}

	public void clickSubmit() 
	{
		loginButton.waitUntilVisible();
		loginButton.click();
    	super.waitForPageLoaded();
	}
	
	public void fillPasswordField(String password) 
	{
		passwordField.waitUntilClickable();
		typeInto(passwordField, password);
	}
	
	public void fillUserIDField(String username) 
	{
		userIDField.waitUntilClickable();
		typeInto(userIDField, username);

	}
	
	public void errorMessageExists(String message) 
	{
		errorMessage.containsText("message");
	}
	
}
