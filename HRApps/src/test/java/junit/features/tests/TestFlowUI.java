package junit.features.tests;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import api.APICase;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.WithTag;
import steps.candidate.CollectEducationDetailsSteps;
import steps.candidate.CollectInformationSteps;
import steps.candidate.CollectWorkHistorySteps;
import steps.candidate.CollectWorkSampleSteps;
import steps.general.AttachmentSteps;
import steps.general.LoginSteps;
import steps.general.StandardSteps;
import steps.general.TopMenuSteps;
import util.casetypes.CurrentCase;

@RunWith(SerenityRunner.class)
@WithTag("UIDemo")
	public class TestFlowUI
	{
	
		@Steps
		LoginSteps login;
		@Steps
		CollectInformationSteps information;
		@Steps
		CollectWorkHistorySteps workHistory;
		@Steps
		CollectEducationDetailsSteps education;
		@Steps
		CollectWorkSampleSteps workSample;
		@Steps
		AttachmentSteps attachments;
		@Steps
		StandardSteps standard;
		@Steps
		TopMenuSteps topMenu;
		
		@Before
		public void setup() 
		{
			APICase.setup();
		}
		
		@After
		public void deletecase() throws Exception 
		{
			driver.close();
			APICase.deleteCase(APICase.caseWorkObject + " " + CurrentCase.caseID);
		}
		
		@Managed(driver = "chrome")
	    WebDriver driver;	
		
		@Test
		public void UI_check_if_attachment_is_attached() throws Exception 
		{
			login.i_am_logged_in_as("standardUser");
			topMenu.i_create_a_new_candidate_case();
			information.i_fill_in_all_the_required_candidate_information("Director", "Test", "Tester", "Test@Tester.com", "2929292", "NLD");
			standard.i_click_submit();	
			workHistory.i_fill_in_a_job1("Job1", "First Company", "1/4/2010", "1/10/2014");
			workHistory.i_fill_in_a_job2("Pega Tester", "Valori", "1/10/2014", "1/1/2018");
			standard.i_click_submit();	
			education.i_fill_in_an_education1("Pega", "Pega CSA", "2015");
			education.i_fill_in_an_education2("ISTQB", "Test engineer", "2010");	
			standard.i_click_submit();	
			workSample.i_upload_a_work_sample();
			standard.i_click_submit();
			attachments.there_are_attachment_attached(1);
			
		}
		
		
	  
	}




