package pages.payrollsetup;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.FindBy;

import pega_objects.interfaces.WebElementPega;
import util.PageTemplate;

public class CollectEmployeeInformation extends PageTemplate {
	
	@FindBy(css="[data-test-id='20160922090823085314573']")
	protected WebElementPega FirstName;
	@FindBy(css="[data-test-id='20160922090823085415817']")
	protected WebElementPega LastName;
	@FindBy(css="[data-test-id='20160922092532026052520']")
	protected WebElementPega Salary;
	@FindBy(css="[name='$PpyWorkPage$pBiweeklySalary']")
	protected WebElementPega BiweeklySalary;
	public void getPageFocus()
	{			
		getDriver().switchTo().defaultContent();
	}
	
	public void typeFirstName(String firstName)
	{
		FirstName.type(firstName);
	}
	
	public void typeLastName(String lastName)
	{
		LastName.type(lastName);
	}
	
	public void typeSalary(String salary) throws InterruptedException
	{
		Salary.type(salary);
		Salary.sendKeys(Keys.TAB);		
	}
	
	public void checkBiWeeklySalary(String expectedBiweeklySalary) throws Exception
	{
		String actualBiweeklySalary = BiweeklySalary.getText();
		if(!actualBiweeklySalary.equals(expectedBiweeklySalary))
				{
				throw new Exception ("Actual Bi-weekly Salary = <" 
				+ actualBiweeklySalary + ">, while <" + 
						expectedBiweeklySalary + "> is expected.");
				}
	}

		
}
