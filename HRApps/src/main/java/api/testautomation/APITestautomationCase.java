package api.testautomation;

import org.json.JSONObject;

import api.APICase;
import enums.CaseType;

public class APITestautomationCase extends APICase {

	CaseType caseType = CaseType.Testautomation;
	public String DemoHiddenField = "Hide Me!";
	public String DemoInteger = "120000";
	public String DemoOptional = "This Text is optional";
	public String DemoRequired = "This Text is required";
	public boolean DemoShowHiddenField = true;	
			

	
	
	public void Demo(String hiddenFieldText, int integerValue, String optionalFieldText, String requiredFieldText, boolean showHiddenField) throws Exception
	{			
		JSONObject TestautomationObject = new JSONObject();
		TestautomationObject.put("DemoShowHiddenField",  String.valueOf(showHiddenField));	
		if(showHiddenField)
		{
			TestautomationObject.put("DemoHiddenField", hiddenFieldText);
		}
		TestautomationObject.put("DemoInteger", String.valueOf(integerValue));
		TestautomationObject.put("DemoOptional", optionalFieldText);
		TestautomationObject.put("DemoRequired", requiredFieldText);	
		updateCase(caseType, caseID, TestautomationObject);
		
	}
	


		
}
