package pega_objects;


import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import pega_objects.interfaces.Autocomplete;


public class AutocompleteImp extends WebElementPegaImp implements Autocomplete
{
	

	public AutocompleteImp(WebDriver driver, ElementLocator locator, long implicitTimeoutInMilliseconds,
			long waitForTimeoutInMilliseconds) {
		super(driver, locator, implicitTimeoutInMilliseconds, waitForTimeoutInMilliseconds);
	}




	@Override
	public void selectValue(String value) {
	
		getElement().sendKeys(value);
		getElement().sendKeys(Keys.DOWN);
		getElement().sendKeys(Keys.TAB);


	}
	

	
		
	

}
