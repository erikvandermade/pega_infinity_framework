package api.payrollsetup;

import net.thucydides.core.annotations.Step;

public class APIPayrollSetupSteps {

	APIPayrollSetupCase payrollSetup;
	
	@Step
    public void i_have_a_new_payroll_setup_case() throws Exception
	{
		createNewPayrollCase();
	}
	
	@Step
    public void i_have_a_payroll_setup_case_in_select_filing_status() throws Exception
	{
		createNewPayrollCase();
		collectEmployeeInformation();
	}
	
	@Step
    public void i_have_a_payroll_setup_case_in_calculate_exemptions() throws Exception
	{
		createNewPayrollCase();
		collectEmployeeInformation();
		selectFilingStatus();
	}

	@Step
    public void i_have_a_payroll_setup_case_in_provide_bank_information() throws Exception
	{
		createNewPayrollCase();
		collectEmployeeInformation();
		selectFilingStatus();
		calculateExemptions();
	}
	
	@Step
    public void i_have_a_payroll_setup_case_in_review_payroll_setup() throws Exception
	{
		createNewPayrollCase();
		collectEmployeeInformation();
		selectFilingStatus();
		calculateExemptions();
		provideBankInformation();
	}
	
	public void i_update_the_employee_information(String firstName, String lastName, String salary) throws Exception 
	{
		payrollSetup.pyFirstName = firstName;
		payrollSetup.pyLastName = lastName;
		payrollSetup.Salary = salary;
		payrollSetup.collectEmployeeInformation();	
	}
	
	public void i_update_the_filing_status(String string) throws Exception 
	{
		payrollSetup.FilingStatus="Married";
		payrollSetup.selectFilingStatus();
	}
	
	public void i_update_the_number_of_dependents(String nrOfDependents) throws Exception 
	{
		payrollSetup.NumberOfDependents=nrOfDependents;
		payrollSetup.calculateExemptions();
		
	}
	
	public void i_update_the_bank_information(String bankName, String routingNumber, String accountNumber) throws Exception 
	{
		payrollSetup.BankName=bankName;
		payrollSetup.RoutingNumber = routingNumber;
		payrollSetup.AccountNumber = accountNumber;
		payrollSetup.provideBankInformation();	
	}
	
	@Step
    public void i_check_that_the_assignment_is_assigned_to(String assignment, String routedTo) throws Exception
	{
		payrollSetup.checkAssignmentIsRoutedTo(assignment, routedTo);
	}
	
	
    private void createNewPayrollCase() throws Exception
	{
		System.err.println("======================= STARTING SUBSTEP: Create new PayrollCase");
		payrollSetup = new APIPayrollSetupCase();
		System.err.println("======================= FINISHING SUBSTEP");
		payrollSetup.createCase(payrollSetup.caseType);
	}
	
    private void collectEmployeeInformation() throws Exception
	{
		System.err.println("======================= SUBSTEP: Collect Employee Information");
		payrollSetup.collectEmployeeInformation();
		System.err.println("======================= FINISHING SUBSTEP");
		payrollSetup.submitCollectEmployeeInformation();
		
	}
 

    private void selectFilingStatus() throws Exception
	{
		System.err.println("======================= STARTING SUBSTEP: Select Filing Status");		
		payrollSetup.selectFilingStatus();
		System.err.println("======================= FINISHING SUBSTEP");
		payrollSetup.submitSelectFilingStatus();
	}
	
    private void calculateExemptions() throws Exception
	{
		System.err.println("======================= STARTING SUBSTEP: Calculate Exemptions");
		payrollSetup.calculateExemptions();
		payrollSetup.submitCalculateExemptions();
	}
	
    private void provideBankInformation() throws Exception
	{
		System.err.println("======================= STARTING SUBSTEP: Provide Bank Information");
		payrollSetup.provideBankInformation();
		System.err.println("======================= FINISHING SUBSTEP");
		payrollSetup.submitProvideBankInformation();
	}






    

	
	
}
