@Cucumberdemo
Feature: Upload Work Sample
In order to check if the candidates work history
I want to be sure that
If the Work Sample is uploaded to the case
The Work Sample is attached to the case

Background:
		Given I have a "Candidate" case in status "UploadWorkSample"
		And I am logged in as "standardUser"

	Scenario: Check if Work Sample is uploaded to the case
		  Given 	I open the case
		  When 		I upload "WorkSample.docx"
		  And 		I click submit
		  Then 		There are 1 attachments of category "WorkSample" uploaded to the case
		  
	Scenario: Check if no Work Sample is uploaded to the case
		  Given 	I open the case
		  And 		I click submit
		  Then 		There are 0 attachments of category "WorkSample" uploaded to the case
		  		  		  	 
		  		  		  	 

		  
		  


		  
	
		