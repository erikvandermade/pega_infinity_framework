package pages.candidate;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import pages.StandardPage;
import pega_objects.interfaces.Dropdown;
import pega_objects.interfaces.WebElementPega;

public class CollectInformation extends StandardPage
{
	@FindBy(how = How.CSS, using = "[data-test-id='201602090624400848477']")
	protected Dropdown appliedPosition;
	
	@FindBy(how = How.CSS, using = "[data-test-id='20160906022815099163281']")
	protected WebElementPega referredByEmployee;

	@FindBy(how = How.CSS, using = "[data-test-id='20160906022815099264140']")
	protected WebElementPega referringEmployee;
	
	@FindBy(how = How.CSS, using = "[data-test-id='201602041104080538163']")
	protected WebElementPega firstName;
	
	@FindBy(how = How.CSS, using = "[data-test-id='201602041104080539993']")
	protected WebElementPega lastName;
	
	@FindBy(how = How.CSS, using = "[data-test-id='201602041104080537141']")
	protected WebElementPega email;
	
	@FindBy(how = How.CSS, using = "[data-test-id='20160928055300071048126']")
	protected WebElementPega TIN;
	
	@FindBy(how = How.CSS, using = "[data-test-id='201602041104080540647']")
	protected WebElementPega homePhone;
	
	@FindBy(how = How.CSS, using = "[data-test-id='201609121507270539811872']")
	protected WebElementPega address;
	
	@FindBy(how = How.CSS, using = "[data-test-id='201609121507270539812137']")
	protected WebElementPega city;
	
	@FindBy(how = How.CSS, using = "[data-test-id='201609121507270540815292']")
	protected WebElementPega state;
	
	@FindBy(how = How.CSS, using = "[data-test-id='201609121547560425199644']")
	protected WebElementPega postalCode;
	
	@FindBy(how = How.CSS, using = "[data-test-id='201609121507270540816111']")
	protected WebElementPega countryCode;
	
	public void select_Position(String position) 
	{
		appliedPosition.selectByVisibleText(position); 
	}
	
	public void click_ReferredByEmployee() 
	{
		referredByEmployee.click();
	}
	
	public void type_ReferringEmployee(String text)
	{
		referringEmployee.type(text);
	}
	
	public void type_FirstName(String text)
	{
		firstName.type(text);
	}
	
	public void type_LastName(String text)
	{
		lastName.type(text);
	}
	
	public void type_Email(String text)
	{
		email.type(text);
	}
	
	public void type_TIN(String text)
	{
		TIN.type(text);
	}
	
	public void type_HomePhone(String text)
	{
		homePhone.type(text);
	}
	
	public void type_Address(String text)
	{
		address.type(text);
	}
	
	public void type_City(String text)
	{
		city.type(text);
	}
	
	public void type_State(String text)
	{
		state.type(text);
	}
	
	public void type_PostalCode(String text)
	{
		postalCode.type(text);
	}

	public void type_CountryCode(String text)
	{
		countryCode.type(text);
	}

}
