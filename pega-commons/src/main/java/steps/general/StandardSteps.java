package steps.general;

import net.thucydides.core.annotations.Step;
import pages.ActionMenu;
import pages.StandardPage;


public class StandardSteps {

	StandardPage standardPage;
	ActionMenu	actionMenu;
	
    @Step
    public void i_click_Save()
    {
    	standardPage.clickSave();
    }
    
    @Step
    public void i_click_submit()
    {
    	standardPage.clickSubmit();
    }
	
    @Step
    public void i_click_cancel()
    {
    	standardPage.clickCancel();
    }
    
    @Step
    public void i_click_action(String action)
    {
    	actionMenu.selectItem(action);
    }
    
    
}
