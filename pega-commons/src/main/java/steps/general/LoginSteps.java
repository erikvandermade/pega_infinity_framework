package steps.general;

import net.thucydides.core.annotations.Step;
import pages.LoginPage;

public class LoginSteps {

	LoginPage loginPage;

    @Step           
    public void i_am_logged_in_as(String userName) throws Exception 
    {	  	
    	loginPage.NavigateAndlogin(userName);
    }
	
}
