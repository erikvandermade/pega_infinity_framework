package api.testautomation;

import net.thucydides.core.annotations.Step;


public class APITestautomationCaseSteps {

	APITestautomationCase testAutomation;
	
	@Step
    public void new_testautomation_case()
	{
		testAutomation = new APITestautomationCase();
		testAutomation.createCase(testAutomation.caseType);
	}
	
	@Step
	public void i_update_the_demo_fields(String hiddenFieldText, int integer, String optionalFieldText, String requiredFieldText, boolean showHiddenField) throws Exception
	{
		testAutomation.Demo(hiddenFieldText, integer, optionalFieldText, requiredFieldText, showHiddenField);
	}
	
	@Step
    public void i_have_a_new_demo_case() throws Exception
	{
		testAutomation = new APITestautomationCase();
		testAutomation.createCase(testAutomation.caseType);
	}
	


	
}
	
	
	
