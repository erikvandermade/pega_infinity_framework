package pages.candidate;

import org.openqa.selenium.support.FindBy;

import pages.StandardPage;
import pega_objects.interfaces.WebElementPega;

public class CollectWorkHistory extends StandardPage
{
	@FindBy(css= "[data-test-id='2015071603054009744126']")
	protected WebElementPega addItem;
	@FindBy(css= "[data-test-id='201507160305400976873']")
	protected WebElementPega deleteItem;
	
	@FindBy(css= "[data-test-id='201904151133180512465'] tr:nth-of-type(2) [data-test-id='2015070709013409384372']")
	protected WebElementPega jobTitle1;
	@FindBy(css= "[data-test-id='201904151133180512465'] tr:nth-of-type(2) [data-test-id='2015070709013409384373']")
	protected WebElementPega company1;
	@FindBy(css= "[data-test-id='201904151133180512465'] tr:nth-of-type(2) [data-test-id='2015070709013409384374']")
	protected WebElementPega startDate1;
	@FindBy(css= "[data-test-id='201904151133180512465'] tr:nth-of-type(2) [data-test-id='2015070709013409384375']")
	protected WebElementPega endDate1;
	
	
	@FindBy(css= "[data-test-id='201904151133180512465'] tr:nth-of-type(3) [data-test-id='2015070709013409384372']")
	protected WebElementPega jobTitle2;
	@FindBy(css= "[data-test-id='201904151133180512465'] tr:nth-of-type(3) [data-test-id='2015070709013409384373']")
	protected WebElementPega company2;
	@FindBy(css= "[data-test-id='201904151133180512465'] tr:nth-of-type(3) [data-test-id='2015070709013409384374']")
	protected WebElementPega startDate2;
	@FindBy(css= "[data-test-id='201904151133180512465'] tr:nth-of-type(3) [data-test-id='2015070709013409384375']")
	protected WebElementPega endDate2;
	
	public void click_addItem()
	{
		addItem.click();
	}
	
	public void click_deleteItem()
	{
		deleteItem.click();
	}
	
	public void type_JobTitle1(String text)
	{
		jobTitle1.type(text);
	}
	
	public void type_Company1(String text)
	{
		company1.type(text);
	}
	
	public void type_StartDate1(String text)
	{
		startDate1.type(text);
	}
	
	public void type_EndDate1(String text)
	{
		endDate1.type(text);
	}
	
	public void type_JobTitle2(String text)
	{
		jobTitle2.type(text);
	}
	
	public void type_Company2(String text)
	{
		company2.type(text);
	}
	
	public void type_StartDate2(String text)
	{
		startDate2.type(text);
	}
	
	public void type_EndDate2(String text)
	{
		endDate2.type(text);
	}
	


}
