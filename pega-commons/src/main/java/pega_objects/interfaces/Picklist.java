package pega_objects.interfaces;

import net.serenitybdd.core.annotations.ImplementedBy;
import pega_objects.PicklistImp;


@ImplementedBy(PicklistImp.class)
public interface Picklist extends WebElementPega
{
	public void selectValue(String Value) throws Exception;
	
}