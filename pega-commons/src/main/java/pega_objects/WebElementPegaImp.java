package pega_objects;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import net.serenitybdd.core.pages.WebElementFacadeImpl;
import net.thucydides.core.webdriver.WebDriverFacade;
import pega_objects.interfaces.WebElementPega;


public class WebElementPegaImp extends WebElementFacadeImpl implements WebElementPega
{
	protected WebDriver driver;
	
	public WebElementPegaImp(WebDriver driver, ElementLocator locator, long implicitTimeoutInMilliseconds,
			long waitForTimeoutInMilliseconds) {
		super(driver, locator, implicitTimeoutInMilliseconds, waitForTimeoutInMilliseconds);
		this.driver = driver;
	}

	@Override
	public void click()
	{	
		Capabilities cap = ((RemoteWebDriver)((WebDriverFacade) driver).getProxiedDriver()).getCapabilities();
		String BrowserName = cap.getBrowserName().toString();
		if (BrowserName.equals("internet explorer"))
	    {
	    	Actions act = new Actions(driver);
			act.doubleClick(getElement()).build().perform();
	    }
	    else
	    {
	        getElement().click();
	    }
	}
	
	

	
		
	

}
