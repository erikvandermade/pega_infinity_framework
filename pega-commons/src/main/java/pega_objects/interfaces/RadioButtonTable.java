package pega_objects.interfaces;

import java.util.List;

import net.serenitybdd.core.annotations.ImplementedBy;
import pega_objects.RadioButtonTableImp;


@ImplementedBy(RadioButtonTableImp.class)
public interface RadioButtonTable extends WebElementPega
{
	public void selectOption(String Option) throws Exception;
	public void checkOptions(List<String> options) throws Exception;	
}