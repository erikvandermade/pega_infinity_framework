package DX;

public class DXField {

	public String fieldID;
	public boolean visible;
	public boolean readOnly; 
	public boolean required;
	public DXFieldType fieldType; 
	public int maxLength;
	public String value;
	public String label;
	
	public DXField (String fieldID)
	{
		this.fieldID=fieldID;
	}
}
