package pages.general;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import pega_objects.interfaces.WebElementPega;
import util.PageTemplate;

public class LeftMenu extends PageTemplate {
	
	@FindBy(how = How.CSS, using=".menu-format-primary-navigation li:nth-child(1)")
	protected WebElementPega myWork;
	@FindBy(how = How.CSS, using=".menu-format-primary-navigation li:nth-child(2)")
	protected WebElementPega MyTeams;
	@FindBy(how = How.CSS, using=".menu-format-primary-navigation li:nth-child(3)")
	protected WebElementPega MyCases;

	@FindBy(how = How.CSS, using=".menu-format-primary-navigation li:nth-child(5)")
	protected WebElementPega calendar;
	@FindBy(how = How.CSS, using=".menu-format-primary-navigation li:nth-child(6)")
	protected WebElementPega pulse;
	@FindBy(how = How.CSS, using=".menu-format-primary-navigation li:nth-child(7)")
	protected WebElementPega tags;
	
	public void getPageFocus()
	{			
		getDriver().switchTo().defaultContent();
	}
	
	public void openMyWork() 
	{
		myWork.waitUntilClickable();
		myWork.click();
      	super.waitForPageLoaded();  
		}
	
	public void openMyTeams() 
	{
		MyTeams.waitUntilClickable();
		MyTeams.click();
     	super.waitForPageLoaded();  
	}
	
	public void openMyCases() 
	{
		MyCases.waitUntilClickable();
		MyCases.click();
     	super.waitForPageLoaded();  
	}
	
	
	public void openCalendar() 
	{
		calendar.waitUntilClickable();
		calendar.click();
     	super.waitForPageLoaded();  
		}
	
	public void openPulse() 
	{
		pulse.waitUntilClickable();
		pulse.click();
	}	
	
	public void openTags() 
	{
		tags.waitUntilClickable();
		tags.click();
	}	
		
}
