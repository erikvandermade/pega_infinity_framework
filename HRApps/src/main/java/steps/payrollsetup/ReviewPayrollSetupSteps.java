package steps.payrollsetup;

import net.thucydides.core.annotations.Step;
import pages.payrollsetup.ReviewPayrollSetup;

public class ReviewPayrollSetupSteps {

	ReviewPayrollSetup reviewPayrollSetup;
	
	@Step           
    public void i_check_the_employee_information(String expectedFirstName, 
    		String expectedLastName, String expectedSalary, String expectedBiweeklySalary) throws Exception
    {
		reviewPayrollSetup.selectTab("Employee Information");
		reviewPayrollSetup.checkFirstName(expectedFirstName);
		reviewPayrollSetup.checkLastName(expectedLastName);
		reviewPayrollSetup.checkSalary(expectedSalary);
		reviewPayrollSetup.checkBiWeeklySalary(expectedBiweeklySalary);
    }
	
	@Step           
    public void i_check_the_filing_status(String expectedFilingStatus) throws Exception
    {
		reviewPayrollSetup.selectTab("Filing Status");
		reviewPayrollSetup.checkFilingStatus(expectedFilingStatus);
    }
	
	@Step           
    public void i_check_the_withholding_information(String expectedNrOfDependents, 
    		String expectedNrOfExemptions, String expectedFinalWithholding) throws Exception
    {
		reviewPayrollSetup.selectTab("Withholding Selection");
		reviewPayrollSetup.checkNrOfDependents(expectedNrOfDependents);
		reviewPayrollSetup.checkNrOfExemptions(expectedNrOfExemptions);
		reviewPayrollSetup.checkFinalWithholding(expectedFinalWithholding);
    }

	@Step           
    public void i_check_the_bank_information(String expectedBankName, String expectedRoutingNumber, 
    		String expectedAccountNumber) throws Exception
    {
		reviewPayrollSetup.selectTab("Bank Information");
		reviewPayrollSetup.checkBankName(expectedBankName);
		reviewPayrollSetup.checkRoutingNumber(expectedRoutingNumber);
		reviewPayrollSetup.checkAccountNumber(expectedAccountNumber);
    }
	

	
	
	
	
}
