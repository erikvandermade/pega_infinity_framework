package util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

	public class Property {
	            
				public static final String baseUrl =  LoadProperty("baseUrl");
				public static final String username = LoadProperty("username");
				public static final String password = LoadProperty("password");
		
	            public static String LoadProperty(String property) {
	            
	            Properties prop = new Properties();
	            InputStream input = null;

	            try {

	                     input = new FileInputStream("config.properties");

	                    // load a property file
	                    prop.load(input);

	                   // fetch values of properties


	             } catch (IOException ex) {
	                   ex.printStackTrace();
	             } finally {
	                  if (input != null) {
	                 try {
	                         input.close();
	                  } catch (IOException e) {
	                        e.printStackTrace();
	                 }
	             }
	        }
				return prop.getProperty(property);

	     }
	 public String getProperty(String property)
	 {
		 return LoadProperty(property);
	 }
	
}