package pages.general;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import util.PageTemplate;

public class TaskWidget extends PageTemplate {
		
	public void openAssignment(String assignment)
	
	{
		String XPATH = "//a[contains(., '" + assignment + "')]";				
		WebElement TaskElement = super.getDriver().findElement(By.xpath(XPATH));
		TaskElement.click();		
	}

}
