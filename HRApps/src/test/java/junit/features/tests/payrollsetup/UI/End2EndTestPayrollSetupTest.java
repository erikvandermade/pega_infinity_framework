package junit.features.tests.payrollsetup.UI;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import api.APICase;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.annotations.WithTags;
import steps.general.InfoTabsSteps;
import steps.general.LoginSteps;
import steps.general.StandardSteps;
import steps.general.TaskWidgetSteps;
import steps.general.TopMenuSteps;
import steps.payrollsetup.CalculateExemptionsSteps;
import steps.payrollsetup.CollectEmployeeInformationSteps;
import steps.payrollsetup.ProvideBankInformationSteps;
import steps.payrollsetup.ReviewPayrollSetupSteps;
import steps.payrollsetup.SelectFilingStatusSteps;
import util.casetypes.CurrentCase;

	@RunWith(SerenityRunner.class)
	@WithTags (
	        {
	        		@WithTag("PayrollSetupUITests"),
	        		@WithTag(name="End 2 End", type="Payroll Setup"),
	        }
	)
	public class End2EndTestPayrollSetupTest 
	{
	@Steps
	LoginSteps login;
	@Steps
	TopMenuSteps topMenu;
	@Steps
	StandardSteps standard;
	@Steps
	CollectEmployeeInformationSteps employee;
	@Steps
	SelectFilingStatusSteps filingStatus;
	@Steps
	CalculateExemptionsSteps exemptions;
	@Steps
	ProvideBankInformationSteps bankinformation;
	@Steps
	ReviewPayrollSetupSteps review;
	@Steps
	InfoTabsSteps infotabs;
	@Steps
	TaskWidgetSteps tasks;
	
	@Before
	public void setup() 
	{
		APICase.setup();
	}
	
	@After
	public void deletecase() throws Exception 
	{
		APICase.deleteCase(APICase.caseWorkObject + " " + CurrentCase.caseID);
	}
	
	@Managed(driver = "chrome")
    WebDriver driver;

	@Test
	public void PayrollSetup_E2E() throws Exception 
	{
	login.i_am_logged_in_as("standardUser");
	topMenu.i_create_a_new_payroll_setup_case();
	employee.i_fill_in_employee_information("John", "Doe", "104000");
	employee.i_check_that_the_biweekly_salary_is("$4,000.00");
	standard.i_click_submit();
	filingStatus.i_select_the_filing_status("Married");
	standard.i_click_submit();
	exemptions.i_fill_in_the_number_of_dependents(1);
	exemptions.i_check_the_calculated_number_of_exemptions(3);
	exemptions.i_check_the_calculated_final_withholding("$125.10");
	standard.i_click_submit();
	bankinformation.i_fill_in_the_bank_information("BNK", "30303030", "42424242");
	standard.i_click_submit();
	standard.i_click_action("Complete enrollment");
	review.i_check_the_employee_information("John", "Doe", "$104,000.00", "$4,000.00");
	review.i_check_the_filing_status("Married");
	review.i_check_the_withholding_information("1", "3", "$125.10");
	review.i_check_the_bank_information("BNK", "30303030", "42424242");
	standard.i_click_submit();
	infotabs.i_check_that_the_case_status_is("Resolved-Completed");
	
	}
	}
	
	
	

