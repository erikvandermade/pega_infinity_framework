package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import net.serenitybdd.core.pages.WebElementFacade;
import util.PageTemplate;

public class ActionMenu extends PageTemplate {
	
	@FindBy(how = How.XPATH, using="//button[contains(., 'Actions')]")
	protected WebElementFacade Actions;
	
	public void selectItem(String action)
	
	{
		scrollIntoView(Actions);
		Actions.click();
		String XPATH = "//span/span[contains(., '" + action + "')]";				
		WebElement ActieElement = super.getDriver().findElement(By.xpath(XPATH));
		ActieElement.click();		
	}
	
	
}
