package enums;

public enum CaseType {

		Candidate ("", "Candidate"),
		PayrollSetup ("", "PayrollSetup"), 
		Testautomation ("", "Testautomation");

		public final String object; 
		public final String label;
		
		CaseType(String object, String label) 
		  {
		        this.object = object;
		        this.label = label;
		  }

		public String object() { return object; }
		public String label() { return label; }

}
