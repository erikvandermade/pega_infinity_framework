package util;

public enum How {
		  CLASS_NAME,
		  CSS,
		  ID,
		  ID_OR_NAME,
		  LINK_TEXT,
		  NAME,
		  PARTIAL_LINK_TEXT,
		  TAG_NAME,
		  TESTID,
		  XPATH,
		  UNSET
		}

