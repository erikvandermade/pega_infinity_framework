package pages.general;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import net.serenitybdd.core.pages.WebElementFacade;
import pega_objects.interfaces.Dropdown;
import pega_objects.interfaces.TextInput;
import util.PageTemplate;

public class ParticipantsWidget extends PageTemplate {
	
	@FindBy(how = How.CSS, using = "a[data-test-id='201501221036030061196576'] i")
	protected WebElementFacade participantsPopup;
	@FindBy(how = How.CSS, using="#modalContent #pyPartyClassPrompt")
	protected Dropdown participantType;
	@FindBy(how = How.CSS, using="tr[pg_subscript='Adviescommissielid '] div>span>#pyFirstName")
	protected TextInput interestedFirstName;
	@FindBy(how = How.CSS, using="tr[pg_subscript='Adviescommissielid '] div>span>#pyAddress")
	protected TextInput interestedAddress;
	@FindBy(how = How.CSS, using="tr[pg_subscript='Adviescommissielid '] div>span>#pyMiddleInitial")
	protected TextInput interestedMiddleInitial;
	@FindBy(how = How.CSS, using="tr[pg_subscript='Adviescommissielid '] div>span>#pyWorkCity")
	protected TextInput interestedWorkCity;
	@FindBy(how = How.CSS, using="tr[pg_subscript='Adviescommissielid '] div>span>#pyLastName")
	protected TextInput interestedLastName;
	@FindBy(how = How.CSS, using="tr[pg_subscript='Adviescommissielid '] div>span>#pyWorkState")
	protected Dropdown interestedWorkState;
	@FindBy(how = How.CSS, using="tr[pg_subscript='Adviescommissielid '] div>span>#pyWorkPhone")
	protected TextInput interestedWorkPhone;
	@FindBy(how = How.CSS, using="tr[pg_subscript='Adviescommissielid '] div>span>#pyWorkPostalCode")
	protected TextInput interestedWorkPostalCode;
	@FindBy(how = How.CSS, using="tr[pg_subscript='Adviescommissielid '] div>span>#pyEmail1")
	protected TextInput interestedEmail;
	@FindBy(how = How.CSS, using="tr[pg_subscript='Adviescommissielid '] div>span>#pyWorkCountry")
	protected TextInput interestedWorkCountry;

	@FindBy(how = How.XPATH, using = "//button[contains(., 'Submit')]")
	protected WebElementFacade submitButton;
	@FindBy(how = How.XPATH, using = "//button[contains(., 'Cancel')]")
	protected WebElementFacade cancelButton;
	
		public void OpenParticipantsPopup()
	{
		participantsPopup.click();
		participantType.waitUntilVisible();
	}
		
		public void checkNotVisible()
		{
			participantType.waitUntilNotVisible();			
		}
	
	public void addInterestedParty(String firstName, String address, String middleInitial,
			String workCity, String lastName, String workState, String workPhone,
			String workPostalCode, String Email, String workCountry) throws Exception
	{
		participantType.selectByVisibleText("Interested");
		interestedFirstName.waitUntilPresent();
		scrollIntoView(interestedFirstName);
		interestedFirstName.type(firstName);
		interestedAddress.type(address);
		interestedMiddleInitial.type(middleInitial);
		interestedWorkCity.type(workCity);
		interestedLastName.type(lastName);
		interestedWorkState.selectByIndex(1);
		interestedWorkPhone.type(workPhone);	
		interestedWorkPostalCode.type(workPostalCode);
		interestedEmail.type(Email);
		interestedWorkCountry.type(workCountry);
	}
	
	public void addInterestedPartyShort(String firstName, String address, String middleInitial,
			String workCity, String lastName, String workState, String workPhone,
			String workPostalCode, String Email, String workCountry) throws Exception
	{
		participantType.selectByVisibleText("Interested");
		interestedFirstName.waitUntilPresent();
		scrollIntoView(interestedFirstName);
		interestedFirstName.type(firstName);
		interestedLastName.type(lastName);
		interestedEmail.type(Email);
	}
	

	
}
