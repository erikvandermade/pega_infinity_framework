package steps.general;

import net.thucydides.core.annotations.Step;
import pages.general.TopMenu;
import util.casetypes.CurrentCase;

public class TopMenuSteps {

	TopMenu topMenu;
	
	@Step           
    public void i_create_a_new_candidate_case()
    {
		 topMenu.clickCreateCandidate();
		 topMenu.waitForDocumentStateReady();
		 CurrentCase.caseID = getCaseIDbyTitle("Candidate");		 
    }
	
	@Step           
    public void i_create_a_new_payroll_setup_case()
    {
		 topMenu.clickCreatePayrollSetup();
		 topMenu.waitForDocumentStateReady();
		 CurrentCase.caseID = getCaseIDbyTitle("Payroll Setup");
    }
	
	@Step           
    public void i_create_a_demo_case()
    {
		 topMenu.clickCreateDemo();
		 topMenu.waitForDocumentStateReady();
		 CurrentCase.caseID = getCaseIDbyTitle("Testautomation");
    }
	
	
	private String getCaseIDbyTitle(String prefix)
	{
		//retrieve caseID from title. By default, the case ID has the casetype as prefix
		String title = topMenu.getPageTitle();
		String caseID = title.substring(prefix.length()+1);
		System.out.println("CaseID = " + caseID);
		return caseID;
	}
	
}
