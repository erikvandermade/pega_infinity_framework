package pega_objects.interfaces;

import net.serenitybdd.core.annotations.ImplementedBy;
import pega_objects.GridTableImp;


@ImplementedBy(GridTableImp.class)
public interface GridTable extends WebElementPega
{
	public void click_Cell(int RowNumber, int ColumnNumber);
	public void click_Link_in_Cell(int RowNumber, int ColumnNumber);
	public int find_Row_With_Text(String Text, int SearchInColumn) throws Exception;
	public void fill_Cell_With_Text(String Text, int RowNumber, int ColumnNumber) throws Exception;
	public void fill_Dropdown(String Text, int RowNumber, int ColumnNumber) throws Exception;
	public void assertValue_Cell(String Text, int RowNumber, int ColumnNumber) throws Exception;
	public void waitUntilContainsText_Cell(String Text, int RowNumber, int ColumnNumber) throws Exception;
	public int countRows();
	
}