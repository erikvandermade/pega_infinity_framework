package enums;

public enum Category
	
	{
		File ("File", "File"),
		WorkSample ("Worksample", "Work sample");

		public final String value; 
		public final String label; 
 
		
		Category(String value, String label)
		  {
		        this.value = value;
		        this.label = label;
		  }
		
		public String value() { return value; }
		public String label() { return label; }
	}
	


