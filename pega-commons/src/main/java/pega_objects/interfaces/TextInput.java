package pega_objects.interfaces;

import net.serenitybdd.core.annotations.ImplementedBy;
import pega_objects.TextInputImp;


@ImplementedBy(TextInputImp.class)
public interface TextInput extends WebElementPega
{
	public void assertLabel(String Text);
	public Boolean isRequired();	
}
