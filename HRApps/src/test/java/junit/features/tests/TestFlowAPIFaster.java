package junit.features.tests;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import api.APICase;
import api.candidate.APICandidateCaseSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.WithTag;
import steps.candidate.CollectWorkSampleSteps;
import steps.general.AttachmentSteps;
import steps.general.MyWorkSteps;
import steps.general.OpenAssignmentUISteps;
import steps.general.StandardSteps;
import util.casetypes.CurrentCase;

@RunWith(SerenityRunner.class)
@WithTag("APIDemo")

	public class TestFlowAPIFaster
	{
		@Steps
		APICandidateCaseSteps candidate;
		@Steps
		OpenAssignmentUISteps assignment;
		@Steps
		MyWorkSteps myWork;
		@Steps
		CollectWorkSampleSteps workSample;
		@Steps
		AttachmentSteps attachments;
		@Steps
		StandardSteps standard;
		
		@Before
		public void setup() 
		{
			APICase.setup();
		}
		
		@After
		public void deletecase() throws Exception 
		{
			driver.close();
			APICase.deleteCase(APICase.caseWorkObject + " " + CurrentCase.caseID);
		}
		
		@Managed(driver = "chrome")
	    WebDriver driver;
		

		@Test
		public void API_check_if_attachment_is_attached() throws Exception 
		{
			candidate.a_candidate_case_in_collect_work_sample();
			assignment.i_open_case_and_assignment("standardUser");
			workSample.i_upload_a_work_sample();
			standard.i_click_submit();
			attachments.there_are_attachment_attached(1);	
		}  
		
		
	}




