package pages.general;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import pega_objects.interfaces.GridTable;
import pega_objects.interfaces.TextInput;
import util.PageTemplate;

public class MyWork extends PageTemplate {


	@FindBy(how = How.ID, using = "pyDescription")
	protected TextInput description;
	@FindBy(how = How.CSS, using = "[data-test-id='201806191351320786122']")
	protected GridTable Worklist;

	
	public void getPageFocus()
	{			
		getDriver().switchTo().frame(1);
	}
	
	public void checkDescriptionlabel()
	{
		description.assertLabel("Beschrijving");
	}
	
	public void fillDescription(String text)
	{
		description.type(text);
	}
	
	public void openCasenumber(String caseNumber) throws Exception
	{
		int rowNumber = Worklist.find_Row_With_Text(caseNumber,3);
		Worklist.click_Cell(rowNumber,2);
	}
	
	public void openFirstCase() throws Exception
	{
		Worklist.click_Cell(1,2);
	}
	
	public void checkCaseStatus(String caseNumber, String status) throws Exception
	{
		int rowNumber = Worklist.find_Row_With_Text(caseNumber,3);
		Worklist.assertValue_Cell(status, rowNumber, 3);
	}

}
