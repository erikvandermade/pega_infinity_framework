package angular.steps.general;

import net.thucydides.core.annotations.Step;
import angular.pages.general.LoginPage;;

public class AngularLoginSteps {
	
	LoginPage login;
	
	@Step           
    public void i_login() throws Exception
    {
		login.Navigate();
		login.login("standarduser");
    }
}
