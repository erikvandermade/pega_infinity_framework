package api.calls;

public class PostCase {

	  public String caseTypeID;
	  public String processID;
	  public String parentCaseID;

	  public void setcaseTypeID(String caseTypeID) 
	  {
	    this.caseTypeID = caseTypeID;
	  }

	  public void setprocessID (String processID) 
	  {
	    this.processID = processID;
	  }

	  public void setparentCaseID (String parentCaseID) 
	  {
	    this.parentCaseID = parentCaseID;
	  }
	  
	  public String getcaseTypeID () 
	  {
	    return caseTypeID;
	  }

	  public String getprocessID () 
	  {
	    return processID;
	  }

	  public String getparentCaseID () 
	  {
	    return parentCaseID;
	  }


}
