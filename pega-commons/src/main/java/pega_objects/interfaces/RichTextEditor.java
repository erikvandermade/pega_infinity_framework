package pega_objects.interfaces;

import net.serenitybdd.core.annotations.ImplementedBy;
import pega_objects.RichTextEditorImp;

@ImplementedBy(RichTextEditorImp.class)
public interface RichTextEditor extends WebElementPega
{
	public void typeText(String Text);
}