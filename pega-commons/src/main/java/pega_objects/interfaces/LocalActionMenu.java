package pega_objects.interfaces;

import net.serenitybdd.core.annotations.ImplementedBy;
import pega_objects.LocalActionMenuImp;



@ImplementedBy(LocalActionMenuImp.class)
public interface LocalActionMenu extends WebElementPega
{
	public void selectMenuItem(String Text) throws Exception;	
}
