package api.calls;

	public class PostAssignment {

		  public String ID;
		  public String actionID;
		  public String requestBody;


		  public void setcaseTypeID(String ID) 
		  {
		    this.ID = ID;
		  }

		  public void setactionID (String actionID) 
		  {
		    this.actionID = actionID;
		  }
		  
		  public void setrequestBody (String requestBody) 
		  {
		    this.requestBody = requestBody;
		  }

		  public String ID () 
		  {
		    return ID;
		  }

		  public String getactionID () 
		  {
		    return actionID;
		  }

		  public String getrequestBody() 
		  {
		    return requestBody;
		  }



}
