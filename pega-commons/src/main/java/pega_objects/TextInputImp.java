package pega_objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import net.serenitybdd.core.annotations.findby.By;
import pega_objects.interfaces.TextInput;

public class TextInputImp extends WebElementPegaImp implements TextInput
{
	
	public TextInputImp(WebDriver driver, ElementLocator locator, long implicitTimeoutInMilliseconds,
			long waitForTimeoutInMilliseconds) {
		super(driver, locator, implicitTimeoutInMilliseconds, waitForTimeoutInMilliseconds);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void assertLabel(String Text) 
	{
	/*	WebElement TextElement = getElement();
		WebElement parent = TextElement.findElement(By.xpath(".."));
		WebElement grandparent = parent.findElement(By.xpath(".."));
		WebElement greatgrandparent = grandparent.findElement(By.xpath(".."));
		WebElement Textblock = greatgrandparent.findElement(By.xpath(".."));
		WebElement Label = Textblock.findElement(By.cssSelector("label span"));
	*/
	}
	
	@Override
	public Boolean isRequired()
	{
		WebElement TextElement = getElement();
		WebElement parent = TextElement.findElement(By.xpath(".."));
		WebElement grandparent = parent.findElement(By.xpath(".."));
		WebElement greatgrandparent = grandparent.findElement(By.xpath(".."));
		WebElement Textblock = greatgrandparent.findElement(By.xpath(".."));
		WebElement Label = Textblock.findElement(By.cssSelector("label span"));
		String LabelClass = Label.getAttribute("class");
		if (LabelClass.contains("iconRequired"))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}