package angular.steps.general;

import net.thucydides.core.annotations.Step;
import angular.pages.general.TopMenu;

public class AngularTopMenuSteps {
	
	TopMenu menu;
	
	@Step           
    public void i_create_a_demo_case() throws Exception
    {
		menu.clickCreateDemo();
    }
		
}
