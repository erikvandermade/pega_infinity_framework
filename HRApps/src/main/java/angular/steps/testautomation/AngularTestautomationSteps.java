package angular.steps.testautomation;

import net.thucydides.core.annotations.Step;
import angular.pages.testautomation.DemoAngular;

public class AngularTestautomationSteps {
	
	DemoAngular demo;
	
	@Step           
    public void i_check_the_visibility_of_the_initial_fields() throws Exception
    {
		demo.checkDemoHiddenFieldVisible(false);
		demo.checkDemoIntegerVisible(true);
		demo.checkDemoOptionalVisible(true);
		demo.checkDemoRequiredVisible(true);
		demo.checkDemoShowHiddenFieldDisplayed(true);
    }
	
	@Step           
    public void i_click_the_show_hidden_field_checkbox() throws Exception
    {
		demo.clickShowHiddenField();
    }
	
	@Step           
    public void i_check_if_the_hidden_field_is_visible(boolean visible) throws Exception
    {
		demo.checkDemoHiddenFieldVisible(true);
    }
		
}
