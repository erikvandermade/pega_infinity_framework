package steps.payrollsetup;

import net.thucydides.core.annotations.Step;
import pages.payrollsetup.CalculateExemptions;

public class CalculateExemptionsSteps {

	CalculateExemptions calculateExemptions;
	
	@Step           
    public void i_fill_in_the_number_of_dependents(int nrOfDependents) throws Exception
    {
		calculateExemptions.typeNumberOfDependents(nrOfDependents);
    }
	
	@Step           
    public void i_check_the_calculated_number_of_exemptions(int expectedNrOfExemptions) throws Exception
    {
		calculateExemptions.checkNumberOfExemptions(expectedNrOfExemptions);
    }
	
	@Step           
    public void i_check_the_calculated_final_withholding(String expectedFinalWithholding) throws Exception
    {
		calculateExemptions.checkFinalWitholding(expectedFinalWithholding);
    }
	

	
	
	
	
}
