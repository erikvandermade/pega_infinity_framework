package pages;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import net.serenitybdd.core.pages.WebElementFacade;
import util.PageTemplate;
import util.casetypes.CurrentCase;


public class StandardPage extends PageTemplate {


	@FindBy(how = How.XPATH, using = "//button[contains(., 'Submit')]")
	protected WebElementFacade submitButton;
	@FindBy(how = How.XPATH, using = "//button[contains(., 'Save')]")
	protected WebElementFacade saveButton;
	@FindBy(how = How.XPATH, using = "//button[contains(., 'Cancel')]")
	protected WebElementFacade cancelButton;
	@FindBy(how = How.CLASS_NAME, using = "case_id")
	protected WebElementFacade CaseIDtext;
	@FindBy(how = How.CLASS_NAME, using = "errorText")
	protected WebElementFacade ErrorText;
	@FindBy(how = How.CSS, using =  "div[data-node-id='pyCaseMessage']")
	protected WebElementFacade CaseMessage;	
	
	public void clickSubmit()
	{	
		scrollIntoView(submitButton);
		submitButton.waitUntilClickable();
		submitButton.click();	
	}
	
	public void clickCancel()
	{
		scrollIntoView(cancelButton);
		cancelButton.waitUntilClickable();
		cancelButton.click();
	}
	
	public void clickSave()
	{
		scrollIntoView(saveButton);
		saveButton.waitUntilClickable();
		saveButton.click();
	}
	
	public void rememberCaseID()
	{
		CaseIDtext.waitUntilPresent();
		String caseID = CaseIDtext.getText();
		caseID = caseID.replace("(","");
		caseID = caseID.replace(")","");
		System.out.println(caseID);
		CurrentCase.caseID = caseID;
	}
	
	public void checkErrorMessage(String text) throws Exception
	{
		if (!ErrorText.containsText(text))
			{
			throw new Exception("Error '" + ErrorText.getText() + "' does not contain the text <" + text + ">");
			}
	}
	
	public void checkMessage(String text)
	{
		CaseMessage.containsText(text);
	}
	
	
}
