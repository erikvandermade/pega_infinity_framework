package steps.payrollsetup;

import net.thucydides.core.annotations.Step;
import pages.payrollsetup.CollectEmployeeInformation;

public class CollectEmployeeInformationSteps {

	CollectEmployeeInformation collectEmployeeInformation;
	
	
	@Step           
    public void i_fill_in_employee_information(String firstName, String lastName, String salary) throws Exception
    {
		collectEmployeeInformation.typeFirstName(firstName);
		collectEmployeeInformation.typeLastName(lastName);
		collectEmployeeInformation.typeSalary(salary);	
    }
	
	@Step           
    public void i_check_that_the_biweekly_salary_is(String expectedBiweeklySalary) throws Exception
    {
		collectEmployeeInformation.checkBiWeeklySalary(expectedBiweeklySalary);	
    }
	
	
	
	
}
