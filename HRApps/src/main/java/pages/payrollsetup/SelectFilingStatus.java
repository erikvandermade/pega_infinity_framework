package pages.payrollsetup;

import org.openqa.selenium.support.FindBy;

import pega_objects.interfaces.Dropdown;
import util.PageTemplate;

public class SelectFilingStatus extends PageTemplate {
	
	@FindBy(css="[data-test-id='2016092210521008717827']")
	protected Dropdown FilingStatus;
	
	public void getPageFocus()
	{			
		getDriver().switchTo().defaultContent();
	}
	
	public void selectFilingStatus(String status)
	{
		FilingStatus.selectByVisibleText(status);
	}
	
	
		
}
