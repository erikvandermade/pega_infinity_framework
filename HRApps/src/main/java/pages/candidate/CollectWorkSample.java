package pages.candidate;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;

import pages.StandardPage;
import pega_objects.interfaces.WebElementPega;

public class CollectWorkSample extends StandardPage
{
	@FindBy(id = "Browsefiles")
	protected WebElementPega attach;
	@FindBy(xpath = "//a[contains(text(),'Download')]")
	protected WebElementPega downloadFile;
	
	
	public void addFile(String fileName) throws Exception
	{	
		attach.waitUntilVisible();
		String fileLocation = System.getProperty("user.dir") + "\\src\\main\\resources\\fileupload\\";
		super.getDriver().findElement(By.cssSelector("input[type='file']")).sendKeys(fileLocation + fileName);
		downloadFile.waitUntilVisible();
	}
	


}
