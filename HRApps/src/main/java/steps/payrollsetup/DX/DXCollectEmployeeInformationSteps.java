package steps.payrollsetup.DX;

import DX.DXAction;
import DX.DXField;
import DX.DXFieldType;
import DX.DXStepsTemplate;
import net.thucydides.core.annotations.Step;

public class DXCollectEmployeeInformationSteps extends DXStepsTemplate {

	public  DXField firstName;
	public  DXField lastName;
	public  DXField salary;
	public  DXField biWeeklySalary;
	
	/*
	public DXCollectEmployeeInformationSteps()
	{
		cOI = new DXAction("CollectEmployeeInformation_0");
		firstName = new DXField("pyFirstName");
		lastName = new DXField("pyLastName");
	}
	*/

	private void initialize()
	{
		DXaction = new DXAction("CollectEmployeeInformation_0");
		initializeFirstName();		
		initializeLastName();
		initializeSalary();
		initializeBiWeeklySalary();
	}

	@Step           
    public void i_check_that_the_initial_fields_have_the_correct_properties() throws Exception
    {
	 initialize();
	 checkproperties(firstName);
	 checkproperties(lastName);
	 checkproperties(salary);
	 checkproperties(biWeeklySalary);
    }
	
	private void initializeLastName() 
	{
		lastName = new DXField("pyLastName");
		lastName.fieldType = DXFieldType.TextInput;
		lastName.visible = true;
		lastName.readOnly = false;
		lastName.required = false;
		lastName.maxLength = 0;
		lastName.label = "Last Name";
		lastName.value = "";	
	}
	
	private void initializeFirstName() 
	{
		firstName = new DXField("pyFirstName");		
		firstName.fieldType = DXFieldType.TextInput;
		firstName.visible = true;
		firstName.readOnly = false;
		firstName.required = false;
		firstName.maxLength = 0;
		firstName.label = "First Name";
		firstName.value = "";		
	}
	
	private void initializeSalary() 
	{
		salary = new DXField("Salary");		
		salary.fieldType = DXFieldType.Currency;
		salary.visible = true;
		salary.readOnly = false;
		salary.required = false;
		salary.maxLength = -1;
		salary.label = "Salary";
		salary.value = "";		
	}
	
	private void initializeBiWeeklySalary() 
	{
		biWeeklySalary = new DXField("BiweeklySalary");		
		biWeeklySalary.fieldType = DXFieldType.Currency;
		biWeeklySalary.visible = true;
		biWeeklySalary.readOnly = true;
		biWeeklySalary.required = false;
		biWeeklySalary.maxLength = 0;
		biWeeklySalary.label = "Bi-weekly salary";
		biWeeklySalary.value = "0";		
	}


	
}
