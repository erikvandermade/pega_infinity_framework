package steps.payrollsetup;

import net.thucydides.core.annotations.Step;
import pages.payrollsetup.SelectFilingStatus;

public class SelectFilingStatusSteps {

	SelectFilingStatus selectFilingStatus;
	
	
	@Step           
    public void i_select_the_filing_status(String filingStatus) throws Exception
    {
		
		selectFilingStatus.selectFilingStatus(filingStatus);
    }
	

	
	
	
	
}
