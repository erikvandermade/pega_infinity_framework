package steps.payrollsetup.DX;

import DX.DXAction;
import DX.DXField;
import DX.DXFieldType;
import DX.DXStepsTemplate;
import net.thucydides.core.annotations.Step;

public class DXCalculateExemptionsSteps extends DXStepsTemplate {

	public DXField NumberOfDependents = new DXField("NumberOfDependents");
	public DXField NumberOfExemptions = new DXField("NumberOfExemptions");
	public DXField FinalWithholding = new DXField("FinalWithholding");


	private void initialize()
	{
		DXaction = new DXAction("CalculateExemptions_0");
		//initializeDXField(field, fieldType, visible, readonly, required, maxLength, label, value);
		initializeDXField(NumberOfDependents, DXFieldType.Integer, true, false, true, 0, "Number of dependents", "");
		initializeDXField(NumberOfExemptions, DXFieldType.Integer, true, true, false, 0, "Number of exemptions", "2");
		initializeDXField(FinalWithholding, DXFieldType.Currency, true, true, false, 0, "Final withholding", "0");
	}

	@Step           
    public void i_check_that_the_initial_fields_have_the_correct_properties() throws Exception
    {
	 initialize();
	 checkproperties(NumberOfDependents);
	 checkproperties(NumberOfExemptions);
	 checkproperties(FinalWithholding);
    }
	
}
