package api.calls;

import java.util.HashMap;

import io.restassured.RestAssured;
import io.restassured.response.ExtractableResponse;
import util.Property;

public class GetData {

		  public String dataPageID;
		  public HashMap<String, String> inputParameters;
		  public static String APIUsername = Property.LoadProperty("username_APITester");
		  public static String APIPassword = Property.LoadProperty("password_APITester");
		  public void setdatapageID(String ID) 
		  {
		    this.dataPageID = ID;
		  }  
	
		  public static ExtractableResponse<io.restassured.response.Response> getDataPage(String dataPageID, HashMap<String, String> parameters)
			{
				GetData data = new GetData();
				data.dataPageID = dataPageID;
				String parameterString = "";
				if (parameters.size()>0)
				{
					parameterString="?";
					for (String key : parameters.keySet()) {
						String value = parameters.get(key);
						parameterString=parameterString + key + "=" + value + "&";
						}
					parameterString = parameterString.substring(0, parameterString.length()-1);
				}
				String path = "/data/"+ data.dataPageID + parameterString;
				ExtractableResponse<io.restassured.response.Response> response = RestAssured.given().auth().basic(APIUsername,APIPassword)
						    .when().get(path)
						    .then().extract();
				return response;
			}


}
