package pega_objects.interfaces;

import net.serenitybdd.core.annotations.ImplementedBy;
import pega_objects.TabListImp;

@ImplementedBy(TabListImp.class)
public interface TabList extends WebElementPega
{
	public void openTab(String TabName);
	public void checkValueInTabForLabel(String TabName, String Label, String ExpectedResult) throws Exception;
	public void checkTabPresent(String TabName) throws Exception;
	public void checkTabNotPresent(String TabName) throws Exception;
	public String returnValue(Integer TabNumber, String Label) throws Exception;
	public void clickValue(Integer TabNumber, String Label) throws Exception;
}