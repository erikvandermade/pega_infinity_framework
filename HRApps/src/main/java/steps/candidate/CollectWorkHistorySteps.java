package steps.candidate;

import net.thucydides.core.annotations.Step;
import pages.candidate.CollectWorkHistory;

public class CollectWorkHistorySteps {

	CollectWorkHistory collectWorkHistory;

    @Step           
    public void i_fill_in_a_job1(String jobTitle, String company, String startDate, String endDate) throws Exception 
    {
       	collectWorkHistory.type_JobTitle1(jobTitle);
    	collectWorkHistory.type_Company1(company);
    	collectWorkHistory.type_StartDate1(startDate);
    	collectWorkHistory.type_EndDate1(endDate);
    }
    
    @Step           
    public void i_fill_in_a_job2(String jobTitle, String company, String startDate, String endDate) throws Exception 
    {
    	collectWorkHistory.click_addItem();
       	collectWorkHistory.type_JobTitle2(jobTitle);
    	collectWorkHistory.type_Company2(company);
    	collectWorkHistory.type_StartDate2(startDate);
    	collectWorkHistory.type_EndDate2(endDate);
    }
    

    
    
	
}
