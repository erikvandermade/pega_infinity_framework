package cucumber.features.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Step;
import pages.LoginPage;
import pages.StandardPage;


public class StandardSteps {

	StandardPage standard;
	LoginPage	loginPage;
	
    @Given("I am logged in as \"(.*)\"")
    @Step
    public void I_Am_Logged_In_as(String userName) throws Exception
    {
    	loginPage.NavigateAndlogin(userName);
    }
	
    @When("I click save")
    @Step
    public void I_Click_Save()
    {
    	standard.clickSave();
    }
    
    @When("I click submit")
    @Step
    public void I_Click_Submit()
    {
    	standard.clickSubmit();
    }
	
    @When("I click cancel")
    @Step
    public void I_Click_Cancel()
    {
    	standard.clickCancel();
    }
}
