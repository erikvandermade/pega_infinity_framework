package pega_objects;

import java.util.List;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import net.serenitybdd.core.annotations.findby.By;
import pega_objects.interfaces.GridTable;
import util.UtilActions;

public class GridTableImp extends WebElementPegaImp implements GridTable
{
	

	public GridTableImp(WebDriver driver, ElementLocator locator, long implicitTimeoutInMilliseconds,
			long waitForTimeoutInMilliseconds) {
		super(driver, locator, implicitTimeoutInMilliseconds, waitForTimeoutInMilliseconds);
		this.driver = driver;
	}

	public void click_Cell(int RowNumber, int ColumnNumber)
	{
		String CssSelector = GridCellSelector(RowNumber, ColumnNumber);
		WebElement row = getElement().findElement(By.cssSelector(CssSelector));
		row.click();
	}
	
	
	public void click_Link_in_Cell(int RowNumber, int ColumnNumber)
	{
		String CssSelector = GridCellSelector(RowNumber, ColumnNumber) + " span";
		WebElement row = getElement().findElement(By.cssSelector(CssSelector));
		row.click();
	}
		
	public int find_Row_With_Text(String Text, int SearchInColumn) throws Exception
	{
		return UtilActions.find_row_with_text(getElement(), SearchInColumn, Text);
	}
	
	public void fill_Cell_With_Text(String Text, int RowNumber, int ColumnNumber) throws Exception
	{
			WebElement GridTable = getElement();
			int searchRow = RowNumber + 1;
					WebElement cell = GridTable.findElement(By.cssSelector("tbody tr:nth-child(" + searchRow + ")" +
							 " td:nth-child(" + ColumnNumber + ") input"));
			cell.clear();
			cell.sendKeys(Text);
	}
	
	public void fill_Dropdown(String Text, int RowNumber, int ColumnNumber) throws Exception
	{
			WebElement GridTable = getElement();
			int searchRow = RowNumber + 1;
					WebElement cell = GridTable.findElement(By.cssSelector("tbody tr:nth-child(" + searchRow + ")" +
							 " td:nth-child(" + ColumnNumber + ") select"));
			List<WebElement> Options = cell.findElements(By.cssSelector("option"));
			boolean optionFound = false;
			for (WebElement Option:Options)
			{	
				if (Option.getText().equals(Text))
				{
					Option.click();
					optionFound = true;
					break;
				}
			}
			try {
				if (optionFound == false)
					{
						throw new Exception("Dropdown option with text " + Text + " not found.");
					}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
			
	public void assertValue_Cell(String Text, int RowNumber, int ColumnNumber) throws Exception
	{
			WebElement GridTable = getElement();
			int searchRow = RowNumber + 1;
					WebElement cell = GridTable.findElement(By.cssSelector("tbody tr:nth-child(" + searchRow + ")" +
							 " td:nth-child(" + ColumnNumber + ") span"));
			if (!cell.getText().equals(Text))
			{
				throw new Exception("Expected Value <" + Text + ">, but found <" + cell.getText() + "> instead.");
			}
	}
	

	public void waitUntilContainsText_Cell(String Text, int RowNumber, int ColumnNumber) throws Exception
	{
			String actualText;
			WebElement GridTable = getElement();
			int searchRow = RowNumber + 1;
			String cssSelector = "tbody tr:nth-child(" + searchRow + ") td:nth-child(" + ColumnNumber + ")";
			WebElement cell = GridTable.findElement(By.cssSelector(cssSelector));
			boolean found = false;
			for(int i=1; i<100; i++)			
			{
				try
				{
				actualText = cell.getText();
				}
				catch (StaleElementReferenceException e)
				{
				cell = GridTable.findElement(By.cssSelector(cssSelector));
				actualText = cell.getText();
				}
				if (actualText.equals(Text))
				{	
					found = true;
					break;
				}
				else
				{
					Thread.sleep(50);
				}
				
			}
			if (!found)
			{
				throw new Exception("Text <" + Text + "> not found in cell.");
			}
		
	}
	
	public void assertText_Cell(String Text, int RowNumber, int ColumnNumber) throws Exception
	{
			WebElement GridTable = getElement();
			int searchRow = RowNumber + 1;
					WebElement cell = GridTable.findElement(By.cssSelector("tbody tr:nth-child(" + searchRow + ")" +
							 " td:nth-child(" + ColumnNumber + ") input"));
			if (!cell.getText().equals(Text))
			{
				throw new Exception("Expected Text <" + Text + ">, but found <" + cell.getText() + "> instead.");
			}
	}
	
	public int countRows()
	{
		WebElement GridTable = getElement();
		List<WebElement> Options = GridTable.findElements(By.cssSelector("tbody tr"));
		int nrRows = Options.size();
		if (nrRows == 3)
				{
				List<WebElement> Check = GridTable.findElements(By.cssSelector("#Grid_NoResults"));
				if (Check.size()==1)	
					{
								return 0;
					}
					{
						return 1;
					}
				}
		return (Options.size()-1); 		
	}
		
	private String GridCellSelector(int Row, int Column)
	{
		Row = Row+1;
		return "tbody tr:nth-child(" + Row + ") td:nth-child(" + Column + ")";
	}
}