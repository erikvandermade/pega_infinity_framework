package DX;

import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import api.APICase;
import util.casetypes.CurrentCase;

public class DXAction {
   
	public JSONArray actionFields;
	public JSONObject actionView;
	private String DXActionID;
	private APICase DXApi;
	
	public DXAction(String actionID)
	{
		this.DXActionID=actionID;
		DXApi = new APICase();
		DXApi.caseID = CurrentCase.caseID;
		DXApi.caseType = CurrentCase.caseType;
		try {
			this.actionView = DXApi.getAssignmentView(DXActionID);
		} catch (Exception e) {
		}
		this.actionFields = getFields(actionView);
	}
	
	public void refresh()
	{
		try {
			this.actionView = DXApi.getAssignmentView(DXActionID);
		} catch (Exception e) {
		}
		this.actionFields = getFields(actionView);
	
	}
	
	public void checkVisibility(DXField field) throws Exception
	{
		checkBooleanProperty(getField(field.fieldID), "visible", field.visible);	
	}
	
	public void checkRequired(DXField field) throws Exception
	{
		checkBooleanProperty(getField(field.fieldID), "required", field.required);	
	}
	
	public void checkReadOnly(DXField field) throws Exception
	{
		checkBooleanProperty(getField(field.fieldID), "readOnly", field.readOnly);	
	}
	
	public void checkValue(DXField field) throws Exception
	{
		checkStringProperty(getField(field.fieldID), "value", field.value);		
	}
	
	public void checkFieldType(DXField field) throws Exception
	{
		JSONObject control = getField(field.fieldID).getJSONObject("control");
		checkStringProperty(control, "type", field.fieldType.value);		
	}
	
	public void checkMaxLength(DXField field) throws Exception
	{
		checkIntProperty(getField(field.fieldID), "maxLength", field.maxLength);		
	}
	
	public  void checkLabel(DXField field) throws Exception 
	{
		JSONObject fieldObject = getField(field.fieldID);
			try {
				checkBooleanProperty(fieldObject, "showLabel", true);
			} catch (JSONException e) {
				// if showLabel doesn't exist, it will be shown, so it's all goooood
			} catch (Exception a){
				throw new Exception(fieldObject.getString("fieldID")+ ": " + "Label is not visible, while it was expected to show <" + field.value + ">");
			}			
			checkStringProperty(fieldObject, "label", field.label);
	}
	
	private void checkIntProperty(JSONObject field, String key, int expectedValue) throws Exception 
	{
		int actualValue = field.getInt(key);
		if (!(actualValue==expectedValue))
				{
			throw new Exception(field.getString("fieldID")+ ": " + key + " is  equal to <" + String.valueOf(actualValue) + ">, while it was expected to be <" + String.valueOf(expectedValue)  + ">");
				};		
	}
	
	private void checkStringProperty(JSONObject field, String key, String expectedValue) throws Exception 
	{
			
		String actualValue = String.valueOf(field.getString(key));
		if (!actualValue.equals(String.valueOf(expectedValue)))
				{
			throw new Exception(field.getString("fieldID")+ ": " + key + " is  equal to <" + actualValue + ">, while it was expected to be <" + expectedValue  + ">");
				}
		
	}
	
	private void checkBooleanProperty(JSONObject field, String key, boolean expectedValue) throws Exception 
	{
			
		boolean actualValue = field.getBoolean(key);
		if (!(actualValue==expectedValue))
				{
			throw new Exception(field.getString("fieldID") +": " + key + " is equal to <" + String.valueOf(actualValue) + ">, while it was expected to be <" + String.valueOf(expectedValue)  + ">");
				}
		
	}
	
	private JSONObject getField(String fieldID) throws Exception
	{
		for (int i=0; i < actionFields.length(); i++) 
		
		{
			JSONObject fieldObject = actionFields.getJSONObject(i);
			String actualFieldID = fieldObject.getString("fieldID");
			if (actualFieldID.equals(fieldID))
			{
				return fieldObject;
			}
			else
			{
				continue;
			}
		}
		throw new Exception("Field <" + fieldID + "> not found");
	}
	
	private JSONArray getFields(JSONObject view)
	{
		JsonParser p = new JsonParser();
		JSONArray fields = new JSONArray();
		JSONArray finalFields = getObjects(p.parse(view.toString()),fields, "field");
		return finalFields;
	}
	
	private JSONArray getObjects(JsonElement jsonElement, JSONArray fields, String type) {	
        if (jsonElement.isJsonArray()) {
            for (JsonElement jsonElement1 : jsonElement.getAsJsonArray()) {
            	getObjects(jsonElement1,fields, type);
            }
        } else {
            if (jsonElement.isJsonObject()) {
                Set<Map.Entry<String, JsonElement>> entrySet = jsonElement
                        .getAsJsonObject().entrySet();
                for (Map.Entry<String, JsonElement> entry : entrySet) {
                    String key1 = entry.getKey();
                    if (key1.equals(type)) {             
                    	JSONObject fieldObject= new JSONObject(jsonElement.toString());
                    	fieldObject = fieldObject.getJSONObject("field");
                    	fields.put(fieldObject);            	
                    }
                    else
                    {
                    	getObjects(entry.getValue(),fields, type);
                    }
                }
            }
        }
		return fields;
    }
	
	
}
