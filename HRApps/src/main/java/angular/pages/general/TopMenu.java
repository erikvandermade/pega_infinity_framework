package angular.pages.general;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import net.serenitybdd.core.pages.WebElementFacade;
import util.PageTemplate;

public class TopMenu extends PageTemplate {
	
	@FindBy(how = How.XPATH, using = "//mat-toolbar-row/button[1]")
	protected WebElementFacade create;
	@FindBy(how = How.XPATH, using="//span[text() = 'Testautomation']")
	protected WebElementFacade demo;
	
	
	public void clickCreateDemo()
	{
		demo.waitUntilPresent();
		create.waitUntilClickable();
		create.click();
		demo.waitUntilVisible();
		demo.click();		
	}

}
