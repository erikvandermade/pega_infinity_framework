package api;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONObject;

import api.calls.GetCase;
import api.calls.GetData;
import api.calls.PostAssignment;
import api.calls.PostCase;
import api.calls.PutCase;
import enums.CaseType;
import enums.Category;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.ExtractableResponse;
import io.restassured.response.Response;
import util.Property;
import util.casetypes.CandidateCase;
import util.casetypes.CurrentCase;

public class APICase {

	public String caseID;
	public JSONObject caseContent;
	public String etag;
	public static String caseWorkObject = "TGB-HRApps-Work";
	public CaseType caseType;
	public String APIUsername = Property.LoadProperty("username_APITester");
	public String APIPassword = Property.LoadProperty("password_APITester");
	public JSONObject assignmentView;
			
	public JSONObject retrieveObjectFromContent(String objectName)
	{
		return caseContent.getJSONObject(objectName);
	}	
	
    public static void setup(){
		RestAssured.useRelaxedHTTPSValidation();	
		RestAssured.baseURI = Property.baseUrl + "/prweb";
		RestAssured.basePath = "/api/v1";
		RestAssured.sessionId = "TestAPI";
    }
		
	public static void deleteCase(String insHandle) throws Exception 
	{
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put("InsHandle", insHandle);
		System.err.println("======================= CLEANING UP... DELETING CASE " + insHandle);
		GetData.getDataPage("D_ResolveandPurgeCase", parameters);
		System.err.println("======================= CASE DELETED ");
	}  
	
	public void checkWorkpartyExists(String party) throws Exception 
	{
		JSONObject workparties = caseContent.getJSONObject("pyWorkParty");
		if(!workparties.has(party))
		{
			throw new Exception("Workparty <" + party + "> does not exists for case " + caseID);
		}
	}
    
	public void createCase(CaseType caseType)
	{
		PostCase post = new PostCase();
		post.caseTypeID = getWorkObject(caseType.label);
		post.parentCaseID = "";
		post.processID = "pyStartCase";
		ExtractableResponse<Response> Response = RestAssured.given().auth().basic(APIUsername,APIPassword).body(post)
				    .when().contentType(ContentType.JSON).post("/cases")
					.then().extract();
		String ID = Response.path("ID");
		if(caseType.object.equals(""))
		{
			caseID = ID.substring(ID.indexOf(" ") + 1);
		}
		else
		{
			caseID = ID.substring(ID.indexOf(caseType.object.toUpperCase()) + caseType.object.length() + 1);
		}
		this.caseType = caseType;
		etag = Response.header("etag");
		System.err.println("=== CASE " + getWorkObject(caseType.object) + " " + caseID + " CREATED ===" );
		storeCaseIDForCaseType(caseType);
		CurrentCase.caseType = caseType;
		CurrentCase.caseID = caseID;
	}


	private static String getWorkObject(String object) 
	{
		if (object.equals(""))
		{
			return caseWorkObject;
		}
		else
		{
			return caseWorkObject + "-" + object;
		}
	}

	public void getCaseContent(String caseID)
	{
		ExtractableResponse<io.restassured.response.Response> response = getResponse(caseID);
		String Response = response.response().asString();
		JSONObject JResponseObject= new JSONObject(Response);
		this.caseID = caseID;
		caseContent = JResponseObject.getJSONObject("content");
		etag = response.header("etag");	
    }
	
	public String getCaseName(String caseID)
	{
		ExtractableResponse<io.restassured.response.Response> response = getResponse(caseID);
		String Response = response.response().asString();
		JSONObject JResponseObject= new JSONObject(Response);
		return JResponseObject.getString("name");		
    }
	
	public String getCaseStatus()
	{
		ExtractableResponse<io.restassured.response.Response> response = getResponse(caseID);
		String Response = response.response().asString();
		JSONObject JResponseObject= new JSONObject(Response);
		return JResponseObject.getString("status");		
    }
	
	public void checkContentValue(String key, String expectedValue) throws Exception
	{
		String actualValue = caseContent.getString(key);
		if (!actualValue.equals(expectedValue))
		{
			throw new Exception ("Content <" + key + "> has value <" + actualValue + ">, but <" + expectedValue + "> was expected");
		}
		
    }
	
	public JSONObject getAssignmentView(String ActionID) throws Exception
	{
		JSONArray assignmentsArray = getAssignments();
		JSONObject assignmentObject = assignmentsArray.getJSONObject(0);
		String AssingmentID = assignmentObject.getString("ID");
 		ExtractableResponse<io.restassured.response.Response> response = RestAssured.given().auth().basic(APIUsername,APIPassword)
 				    .when().contentType(ContentType.JSON).get("/assignments/"+ AssingmentID + "/actions/" + ActionID)
 				    .then().extract();
 		String Response = response.response().asString();	
		JSONObject assignmentView = new JSONObject(Response);
		this.assignmentView = assignmentView;
		return assignmentView;
	}
	
	public JSONArray getChildCases(String caseID) throws Exception
	{
		ExtractableResponse<io.restassured.response.Response> response = getResponse(caseID);
		String Response = response.response().asString();
		JSONObject JResponseObject= new JSONObject(Response);
		this.caseID = caseID;
		try
		{
			return JResponseObject.getJSONArray("childCases");
		}
		catch (Exception e)
		{
			throw new Exception ("No child cases found for Case " + caseID);
		}	
    }
	
	public JSONObject returnAllCasesForUser()
	{
		ExtractableResponse<io.restassured.response.Response> response = RestAssured.given().auth().basic(APIUsername,APIPassword)
				    .when().get("/cases/")
				    .then().extract();
		String Response = response.response().asString();
		JSONObject JResponseObject= new JSONObject(Response);
		return JResponseObject;
    }
    
    public void setCaseContent(CaseType caseType, String caseID)
	{
    	this.caseType = caseType;
    	getCaseContent(caseID);
    }
 
	
	public void updateCaseContent(CaseType zaaktype, String caseID) throws Exception
 	{
		PutCase put = new PutCase();
 		put.ID = getWorkObject(caseType.object) + " " + caseID;		
 		put.If_Match = this.etag;
 		put.requestBody = "{ \"content\": " + this.caseContent +"}";	
 		ExtractableResponse<io.restassured.response.Response> response = RestAssured.given().auth().basic(APIUsername,APIPassword)
 					.header("If-Match",put.If_Match)
 					.body(put.requestBody)
 				    .when().put("/cases/"+ put.ID)
 				    .then().extract();
 		if (!(response.statusCode()==204))
 		{
 			throw new Exception ("Updating case through api went wrong (Errorcode " + response.statusCode() + ") (Case:" + caseID  +")");
 		}
     }
    
    
	public void updateCase(CaseType caseType, String caseID, JSONObject content) throws Exception
 	{
 		PutCase put = new PutCase();
 		put.ID = getWorkObject(caseType.object)+ " " + caseID;		
 		put.If_Match = this.etag;
 		put.requestBody = "{ \"content\": " + content +"}";	
 		ExtractableResponse<io.restassured.response.Response> response = RestAssured.given().auth().basic(APIUsername,APIPassword)
 					.header("If-Match",put.If_Match)
 					.body(put.requestBody)
 				    .when().put("/cases/"+ put.ID)
 				    .then().extract();
 		if (!(response.statusCode()==204))
 		{
 			throw new Exception ("Updating case through api went wrong (Errorcode " + response.statusCode() + ") (Case:" + caseID  +")");
 		}
 		etag = response.header("etag");
     }
	
	public void performCaseAction(CaseType caseType, String caseID, String actionID, String content) throws Exception
 	{
 		PutCase put = new PutCase();
 		put.ID = getWorkObject(caseType.object)+ " " + caseID;
 		put.actionID = actionID;
 		put.If_Match = this.etag;
 		put.requestBody = "{ \"content\": " + content +"}";	
 		ExtractableResponse<io.restassured.response.Response> response = RestAssured.given().auth().basic(APIUsername,APIPassword)
 					.header("If-Match",put.If_Match)
 					.body(put.requestBody)
 				    .when().put("/cases/"+ put.ID + "?actionID=" + put.actionID)
 				    .then().extract();
 		if (!(response.statusCode()==204))
 		{
 			throw new Exception ("Updating case through api went wrong (Errorcode " + response.statusCode() + ") (Case:" + caseID  +")");
 		}
     }
		    
	public void loadResponse()
		{
		ExtractableResponse<io.restassured.response.Response> response = RestAssured.given().auth().basic(APIUsername,APIPassword).    		
        when().get("/cases").
        then().extract();   
        String Response = response.response().asString();
        int start = Response.indexOf("[");
        int end = Response.indexOf("]");
        Response = Response.substring(start,end+1);
		
	    }
			
	public void performAssignmentAction(String AssignmentName, String ActionID, String content) throws Exception
		{
			PostAssignment post = new PostAssignment();
			post.ID = getAssignmentID(AssignmentName);
			post.actionID = ActionID;
			if (content.equals(""))
			{
				content ="{}";
			}
			
			post.requestBody = "{ \"content\": " + content +"}";	
			ExtractableResponse<io.restassured.response.Response> response = RestAssured.given().auth().basic(APIUsername,APIPassword)
						.body(post.requestBody)
					    .when().post("/assignments/" + post.ID + "?actionID=" + ActionID)
					    .then().extract();
			if (!(response.statusCode()==200))
	 		{
	 			throw new Exception ("Performing assignment through api went wrong (Errorcode " + response.statusCode() + ") (Case:" + caseID  +")");
	 		}
			saveHeaderEtag(caseID);
		}
	
	public void performLocalAction(String action, String content) throws Exception
	{
		String assignmentID = getAssignmentIDforAction(action);
		performAssignmentAction(assignmentID, action, content);
	}
	
	public void checkAssignmentNotExists(String assignmentName) throws Exception
	{
			JSONArray assignmentsArray = getAssignments();
			for (int i=0; i < assignmentsArray.length(); i++) 
			{
				JSONObject assignmentObject = assignmentsArray.getJSONObject(i);
				String actualAssignmentName = assignmentObject.getString("name");
				if (actualAssignmentName.equals(assignmentName))
				{
					throw new Exception("Assignment <" + assignmentName + " found, while it was not expected to exist (Case:" + caseID  +")");
				}
				else
				{
					continue;
				}
			}
		
		}	
	
	public void checkAssignmentExists(String assignmentName) throws Exception
	{
			JSONArray assignmentsArray = getAssignments();
			boolean found = false;
			for (int i=0; i < assignmentsArray.length(); i++) 
			{
				JSONObject assignmentObject = assignmentsArray.getJSONObject(i);
				String actualAssignmentName = assignmentObject.getString("name");
				if (actualAssignmentName.equals(assignmentName))
				{
					found = true;
					break;
				}
				else
				{
					continue;
				}
			}
			if (!found)
			{
			throw new Exception("Assignment <" + assignmentName + " not found, while it was expected to exist (Case:" + caseID  +")");
			}
		}

	public JSONArray getAssignments()
	{
		GetCase get = new GetCase();
 		get.ID = getWorkObject(caseType.object)+ " " + caseID;
 		ExtractableResponse<io.restassured.response.Response> response = RestAssured.given().auth().basic(APIUsername,APIPassword)
 				    .when().contentType(ContentType.JSON).get("/cases/"+ get.ID)
 				    .then().extract();
 		String Response = response.response().asString();
		JSONObject JResponseObject= new JSONObject(Response);
		JSONArray assignmentsArray = (JResponseObject.getJSONArray("assignments"));
		return assignmentsArray;
	}
	
	public void checknrCorrespondence(int aantalBijlagen) throws Exception 
		{
			getCaseContent(caseID);
			JSONArray Correspondentielijst = caseContent.getJSONArray("Correspondentielijst");
			if (Correspondentielijst.length()+1 !=aantalBijlagen)
				{
					throw new Exception ("Number of attachments is <" + (Correspondentielijst.length()+1) + ">, terwijl "
							+ "<"+ aantalBijlagen +"> worden verwacht (Case:" + caseID  +")");
				}
	
		}
	
	public String getAssignmentID(String name) throws Exception
	{
		JSONArray assignmentsArray = getAssignments();
		for (int i=0; i < assignmentsArray.length(); i++) 
		{
			JSONObject assignmentObject = assignmentsArray.getJSONObject(i);
			String assignmentName = assignmentObject.getString("name");
			if (assignmentName.equals(name))
			{
				String ID = assignmentName = assignmentObject.getString("ID");
				return ID;
			}
			else
			{
				continue;
			}
		}
		
			throw new Exception("Assignment not found (Case:" + caseID  +")");
	
	}
	
	private String getAssignmentIDforAction(String actionName) throws Exception
	{
		JSONArray assignmentsArray = getAssignments();
		for (int i=0; i < assignmentsArray.length(); i++) 
		{
			JSONObject assignmentObject = assignmentsArray.getJSONObject(i);
			JSONArray actionsArray = assignmentObject.getJSONArray("actions");
			
			for (int j=0; j < actionsArray.length(); j++) 
			{
				JSONObject actionObject = actionsArray.getJSONObject(j);
				String foundActionName = actionObject.getString("name");
				if (foundActionName.equals(actionName))
				{
					String ID = assignmentObject.getString("ID");
					return ID;
				}
				else
				{
					continue;
				}
			}
		}
		
			throw new Exception("No assignment found that has the action <" + actionName + "> (Case:" + caseID  +")");
	
	}

	
	private void saveCaseContent(String caseID)
	{
		GetCase get = new GetCase();
		get.ID = getWorkObject(caseType.object)+ " " + caseID;
		ExtractableResponse<io.restassured.response.Response> response = RestAssured.given().auth().basic(APIUsername,APIPassword)
			    .when().get("/cases/"+ get.ID)
			    .then().extract();
		String Response = response.response().asString();
		JSONObject JResponseObject= new JSONObject(Response);
		this.caseContent = JResponseObject.getJSONObject("content");
    }
	

   
	private void saveHeaderEtag(String caseID)
 	{
		GetCase get = new GetCase();
 		get.ID = getWorkObject(caseType.object)+ " " + caseID;
 		ExtractableResponse<io.restassured.response.Response> response = RestAssured.given().auth().basic(APIUsername,APIPassword)
 				    .when().contentType(ContentType.JSON).get("/cases/"+ get.ID)
 				    .then().extract();
 		etag = response.header("etag");
     }
	
	public void checkStatus(String expectedStatus) throws Exception 
	{
	getCaseContent(caseID);
	String status = caseContent.getString("pyStatusWork");
	if (!status.equals(expectedStatus))
		{
			throw new Exception ("Status is <" + status + ">, terwijl "
					+ "<"+ expectedStatus +"> als status wordt verwacht (Case:" + caseID  +")");
		}
	}
	
	public void checkLabel(String expectedLabel) throws Exception 
	{
	getCaseContent(caseID);
	String label = caseContent.getString("pyLabel");
	if (!label.equals(expectedLabel))
		{
			throw new Exception ("Label is <" + label + ">, terwijl "
					+ "<"+ expectedLabel +"> als label wordt verwacht (Case:" + caseID  +")");
		}
	}

    private void storeCaseIDForCaseType(CaseType caseType) 
    {
        if(caseType.equals(CaseType.Candidate))
	       {
	    	   CandidateCase.caseID = caseID;
	       }
	}
    
  public void checknrOfAttachments(int number) throws Exception 
  {  	
    	Map<String, String> parameters = new HashMap<String, String>();
    	parameters.put("LinkRefFrom",caseContent.getString("pzInsKey"));
    	JSONObject content = getDatapage("D_AttachmentList", parameters);
    	int nrOfAttachments = Integer.parseInt(content.getString("pxResultCount"));
    	if (nrOfAttachments!=number)
    	{
    		throw new Exception ("Expected a total of <" + number + "> attachments, but "
    				+ "there are <" + nrOfAttachments + "> attachments instead (Case:" + caseID  +")");
    	}
	
	}

    public void checkAttachments(Category category, int number) throws Exception {  	
    	Map<String, String> parameters = new HashMap<String, String>();
    	parameters.put("LinkRefFrom",caseContent.getString("pzInsKey"));
    	JSONObject content = getDatapage("D_AttachmentList", parameters);	
    	JSONArray attachments = content.getJSONArray("pxResults");
    	int nrOfAttachmentsInCategory = 0;
    	for (int i=0; i < attachments.length(); i++) 
		{
			JSONObject attachment = attachments.getJSONObject(i);
			String categoryName = attachment.getString("pyCategory");
			if (categoryName.equals(category.label))
			{
				nrOfAttachmentsInCategory = nrOfAttachmentsInCategory + 1; 
			}
		}
    	if (nrOfAttachmentsInCategory!=number)
    	{
    		throw new Exception ("Expected a total of <" + number + "> attachments with category <"
    				+ category +">, but there are <" + nrOfAttachmentsInCategory + "> attachments instead (Case:" + caseID  +")");
    	}	
	}

    private JSONObject getDatapage (String pageName, Map<String, String> parameters) 
    {	
    	GetCase get = new GetCase();
 		get.ID =  pageName;
    			for (Entry<String, String> parameter : parameters.entrySet())
    			{
    				get.ID  = get.ID + "?" + parameter.getKey() + "=" + parameter.getValue() + "&";
    			}
    	get.ID = get.ID.substring(0, get.ID.length()-1);
 		ExtractableResponse<io.restassured.response.Response> response = RestAssured.given().auth().basic(APIUsername,APIPassword)
 				    .when().contentType(ContentType.JSON).get("/data/"+ get.ID)
 				    .then().extract();
 		String Response = response.response().asString();
		JSONObject JResponseObject= new JSONObject(Response);
		return JResponseObject;	
	}

	public void controleerCaseStatus(String status) throws Exception 
	{
		String actualStatus = getCaseStatus();
		if (!actualStatus.equals(status))
		{
			throw new Exception ("status is equal to <" + actualStatus + ">, while <" + status + "> was expected.");
		}
	}
	
	public void countWorkparties(Integer amount) throws Exception 
	{
		JSONObject workparties = caseContent.getJSONObject("pyWorkParty");
		if (workparties.has("Owner"))
		{
			amount = amount +1;
		}
			if(!(workparties.length()==amount))
			{
				if (workparties.has("Owner"))
				{	
					throw new Exception("Nr of workparties equals <" + (workparties.length()-1) + ">, while <" + (amount-1) + "> is expected");
				}
				else
				{
					throw new Exception("Nr of workparties equals <" + workparties.length() + ">, while <" + amount + "> is expected");
				}
			}	
	}
	
	public void checkPartyProperty(String party, String property, String expectedValue) throws Exception 
	{
		JSONObject workparties = caseContent.getJSONObject("pyWorkParty");
		JSONObject workparty = workparties.getJSONObject(party);
		String actualValue = workparty.getString(property);
		if(!actualValue.equals(expectedValue))
		{
			throw new Exception("Expected value <" + expectedValue + "> for " + property
					+ " of " + party + ", but found <" + actualValue + "> instead");				
		}		
	}
	
	public void checkPartyPropertyNotExists(String party, String property) throws Exception 
	{
		JSONObject workparties = caseContent.getJSONObject("pyWorkParty");
		JSONObject workparty = workparties.getJSONObject(party);
		if(workparty.has(property))
		{
			throw new Exception("Property <" + property + "> found, but was expected not to exist");
		}		
	}
	
	public void checkAssignmentIsRoutedTo(String assignmentName, String routedTo) throws Exception 
	{
				JSONArray assignmentsArray = getAssignments();
				boolean found = false;
				for (int i=0; i < assignmentsArray.length(); i++) 
				{
					JSONObject assignmentObject = assignmentsArray.getJSONObject(i);
					String actualAssignmentName = assignmentObject.getString("name");
					if (actualAssignmentName.equals(assignmentName))
					{
						found = true;
						String actualRouting = assignmentObject.getString("routedTo");
						if (!actualRouting.equals(routedTo))
								{
								throw new Exception("Assignment <" + assignmentName + 
										" was expected to be routed to > " + routedTo + 
										"<but it was routed to <" + actualRouting + "> instead");
								}
						break;
					}
					else
					{
						continue;
					}
				}
				if (!found)
				{
					throw new Exception("Assignment <" + assignmentName + 
							"> not found, while it was expected to exist (Case:" + caseID  +")");
				}
			
					
	}
	
	private ExtractableResponse<io.restassured.response.Response> getResponse(String caseID)
	{
		GetCase get = new GetCase();
		get.ID = getWorkObject(caseType.object)+ " " + caseID;
		ExtractableResponse<io.restassured.response.Response> response = RestAssured.given().auth().basic(APIUsername,APIPassword)
				    .when().get("/cases/"+ get.ID)
				    .then().extract();
		return response;
	}

	
}
