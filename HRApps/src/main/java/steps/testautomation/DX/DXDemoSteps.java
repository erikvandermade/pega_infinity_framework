package steps.testautomation.DX;

import DX.DXAction;
import DX.DXField;
import DX.DXFieldType;
import DX.DXStepsTemplate;
import net.thucydides.core.annotations.Step;

public class DXDemoSteps extends DXStepsTemplate {

	public DXField DemoHiddenField = new DXField("DemoHiddenField");
	public DXField DemoInteger = new DXField("DemoInteger");
	public DXField DemoOptional = new DXField("DemoOptional");
	public DXField DemoRequired = new DXField("DemoRequired");
	public DXField DemoShowHiddenField = new DXField("DemoShowHiddenField");

	private void initialize()
	{
		DXaction = new DXAction("Demo");
		//initializeDXField(field, fieldType, visible, readonly, required, maxLength, label, value);
		initializeDXField(DemoHiddenField, DXFieldType.TextInput, false, false, false, 0, "Hidden field", "");
		initializeDXField(DemoInteger, DXFieldType.TextInput, true, false, false, 0, "Integer", "");
		initializeDXField(DemoOptional, DXFieldType.TextInput, true, false, false, 0, "Optional field", "");
		initializeDXField(DemoRequired, DXFieldType.TextInput, true, false, true, 0, "Required field", "");
		initializeDXField(DemoShowHiddenField, DXFieldType.Checkbox, true, false, false, 0, "Show hidden field", "false");
		
	}

	@Step           
    public void i_check_that_the_initial_fields_have_the_correct_properties() throws Exception
    {
	 initialize();
	 checkproperties(DemoHiddenField);
	 checkproperties(DemoInteger);
	 checkproperties(DemoOptional);
	 checkproperties(DemoRequired);
	 checkproperties(DemoShowHiddenField);
	 
    }
	
}
