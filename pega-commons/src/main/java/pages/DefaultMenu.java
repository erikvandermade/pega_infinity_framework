package pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import net.serenitybdd.core.pages.WebElementFacade;
import util.PageTemplate;

public class DefaultMenu extends PageTemplate
{

	//Create Menu
	@FindBy(how = How.XPATH, using = ".//a[contains(@aria-label, 'menu Create')]")
	protected WebElementFacade createButton;
	@FindBy(how = How.XPATH, using = ".//a[contains(@data-click,'createNewWork')]")
	protected WebElementFacade newCaseMenuItem;
	
	//Search bar
	@FindBy(how = How.XPATH, using = ".//*[@data-test-id='2015030515570700545405']")
	protected WebElementFacade searchField;
	@FindBy(how = How.XPATH, using = ".//*[@data-test-id='2015030515570700546328']")
	protected WebElementFacade searchLogo;
	@FindBy(how = How.XPATH, using = ".//*[@data-test-id='20150212053008079033139']")
	protected WebElementFacade searchResult;
	@FindBy(how = How.XPATH, using = ".//*[@data-test-id='20150212053008079033139']")
	protected List<WebElement> searchResultList;
	
	//User menu
	@FindBy(how = How.XPATH, using = ".//*[@data-test-id='2014100609491604327565']")
	protected WebElementFacade mainMenu;	
	@FindBy(how = How.XPATH, using = ".//a[contains(@data-click,'logOff')]")
	protected WebElementFacade logOffMenuItem;
	
	@FindBy(how = How.XPATH, using  = ".//*[@data-test-id='20150212065202013617599']")
	protected WebElementFacade searchResultFieldVerify;
	
	// basic page functions	
	public void fillSearchField(String search)
	{;
		searchField.waitUntilClickable();
		typeInto(searchField, search);
	}
	
	public void clickSearch()
	{
		searchLogo.waitUntilClickable();
		searchLogo.click();
		searchResultFieldVerify.waitUntilPresent();
	}

	public List<WebElement> getSearchResultList()
	{	
		return searchResultList;
	}

	public void clickSearchResult() 
	{	
		searchResult.waitUntilClickable();
		searchResult.click();
		super.waitForPageLoaded();
	}
	
    public void clickCreateButton()
    {
    	createButton.waitUntilClickable();
    	createButton.click();
    	//Some wait synchronisation step should be included to verify the popup menu being loaded	
    }
    
    public void clickNewCaseMenuItem()
    {
    	newCaseMenuItem.waitUntilClickable();
    	newCaseMenuItem.click();
    	super.waitForPageLoaded();
    }
    
    public void checkThatCreateIsShown()
    {
    	createButton.isVisible();
    }
    

    

    
}
