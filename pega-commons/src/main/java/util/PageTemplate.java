package util;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.webdriver.WebDriverFacade;

public class PageTemplate extends PageObject
{
	@FindBy(css =  "div[data-node-id='pyCaseContentDetails'] a:nth-child(1)")
	protected WebElementFacade CaseStatus;
	
	@FindBy(id =  "PegaRULESErrorFlag")
	protected WebElementFacade ErrorFlag;
	
	@FindBy(className = "iconError")
	protected WebElementFacade ErrorIcon;


	public void waitForLoad() 
    {
    	try 
    	{
    		withTimeoutOf(2, TimeUnit.SECONDS).waitFor(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//IMG[@alt='Loading']")));
    		try 
    		{	
    			withTimeoutOf(10, TimeUnit.SECONDS).waitFor(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//IMG[@alt='Loading']")));
    		}
    		catch(WebDriverException e)
    		{
    		}
		} 
    	catch (WebDriverException e) 
    	{
		}
	}
    
    //Wait for UI Mask
    public void waitForUImask() 
    {
    	try 
    	{
    		waitFor(ExpectedConditions.invisibilityOfElementLocated(By.id("pega_ui_mask")));
		}
    	catch (WebDriverException e) 
    	{
		}
	}
    
    //Can be used from 7.2.2 onwards!
    // Not for 8 anymore
    /*
    public void waitForDocumentready() 
    {		
        	//Can be used from 7.2.2 onwards!
    		waitFor(ExpectedConditions.attributeContains(By.className("document-statetracker"), "data-state-busy-status", "busy"));
    		waitFor(ExpectedConditions.attributeContains(By.className("document-statetracker"), "data-state-busy-status", "ready"));
	}
    
    public void waitForAjaxStatusready() 
    {		
        	//Can be used from 7.2.2 onwards!
    		waitFor(ExpectedConditions.attributeContains(By.className("document-statetracker"), "data-state-ajax-status", "busy"));
    		waitFor(ExpectedConditions.attributeContains(By.className("document-statetracker"), "data-state-ajax-status", "none"));
	}	
	*/
    
    //From Pega 8 onward!
    public void waitForDocumentStateReady() 
    {		
    		withTimeoutOf(10, TimeUnit.SECONDS).waitFor(ExpectedConditions.attributeContains(By.className("document-statetracker"), "data-state-busy-status", "busy"));
    		withTimeoutOf(10, TimeUnit.SECONDS).waitFor(ExpectedConditions.attributeContains(By.className("document-statetracker"), "data-state-busy-status", "none"));
	}
    // End Pega 8 onward
    
    public String getPageTitle()
    {
    	return getDriver().getTitle();
    }

    
    //Wait for a page to be loaded
    public void waitForPageLoaded()
    {
    	
        ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() 
        {
            public Boolean apply(WebDriver driver)
            {
                return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
            }   
        };
        try
        {
        	withTimeoutOf(10, TimeUnit.SECONDS).waitFor(expectation);
        }
        catch(Throwable error)
        {
        }
    }
    
    public void scrollIntoView(WebElementFacade element) 
    {
    	UtilActions.scrollIntoView(element, getDriver());
	}
    
        public void assertCaseAssignment(String Text) throws Exception
    {
        	{
        		CaseStatus.waitUntilVisible();
        		boolean found = false;
        		for (int i = 0; i < 50; i++)
        		{
        			String actualText = CaseStatus.getText();
        			if (actualText.equals(Text))
        			{
        				found = true;
        				break;				
        			}
        		Thread.sleep(100);
        		}
        		if (!found)
        				{
        				throw new Exception("Incorrecte case status, status is gelijk aan <" + CaseStatus.getText() + ">, terwijl <" + Text + "> wordt verwacht." );
        				}
        	}	
        		
       
    }
    
        public boolean BrowserIsInternetExplorer()
        {
	    	Capabilities cap = ((RemoteWebDriver)((WebDriverFacade) getDriver()).getProxiedDriver()).getCapabilities();
	    	String BrowserName = cap.getBrowserName().toString();
	    	if (BrowserName.equals("internet explorer"))
	        {
	    		return true;
	        }	
	    	else
	    	{
	    		return false;
	    	}
        }
        
        
        public void clickCaseStatus()
	    {
	        	CaseStatus.click();        		       
	    }
        
        public void refresh()
	    {
	        	super.getDriver().navigate().refresh();   
	        	waitForPageLoaded();
	    }
        
    	public void checkErrorFlag(String expectedErrorText) throws Exception 
    	{
    		String actualErrorText = ErrorFlag.getText(); 
    		if (!actualErrorText.equals(expectedErrorText))
    		{
    			throw new Exception ("ErrorFlag contains text <" + actualErrorText + ">, while <" + expectedErrorText + "> was expected.");
    		}
    	}
    	
      	public void checkErrorIconBelowElement(String expectedErrorText, WebElementFacade Element) throws Exception 
    	{
    		String actualErrorText = ErrorIcon.getText(); 
    		if (!actualErrorText.equals(expectedErrorText))
    		{
    			throw new Exception ("ErrorIcon contains text <" + actualErrorText + ">, while <" + expectedErrorText + "> was expected.");
    		}
    		long ErrorIconTopPosition = UtilActions.getElementTopPosition(ErrorIcon, getDriver());
    		long ElementBottomPosition = UtilActions.getElementBottomPosition(Element, getDriver());
    		if((ErrorIconTopPosition - ElementBottomPosition) > 20)
    		{
    			throw new Exception ("ErrorIcon is not below the expected element");
    		}	
    	}


}
