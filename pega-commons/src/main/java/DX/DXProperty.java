package DX;

public enum DXProperty 
{
	FieldType,
	Visibility,
	ReadOnly,
	Required,
	MaxLength,
	Value,
	Label
}
