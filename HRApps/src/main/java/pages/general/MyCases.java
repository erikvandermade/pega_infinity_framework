package pages.general;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import pega_objects.interfaces.GridTable;
import util.PageTemplate;

public class MyCases extends PageTemplate {

	@FindBy(how = How.CSS, using =  "table[summary='']")
	protected GridTable caseList;
	
	public void getPageFocus()
	{			
		getDriver().switchTo().frame(1);
	}
		
	public void openCasenumber(String casenumber) throws Exception
	{
		int rowNumber = caseList.find_Row_With_Text(casenumber,2);
		caseList.click_Cell(rowNumber,2);
	}
	
	public void openFirstCase() throws Exception
	{
		caseList.click_Cell(1,2);
	}
	
}